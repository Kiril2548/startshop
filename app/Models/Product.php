<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'desc',
        'slug',
        'category_id',
        'discount_id',
        'status',
        'count',
        'price_nds',
        'price_me',
        'price_opt',
        'price',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function discount()
    {
        return $this->belongsTo(Discounts::class);
    }

    public function imgs()
    {
        return $this->hasMany(ProductImg::class);
    }

    public function params()
    {
        return $this->hasMany(ProductParams::class);
    }
}
