<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ArticleComment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id',
        'user_id',
        'comment',
        'likes',
        'views'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
