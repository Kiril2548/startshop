<?php

namespace App\Console\Commands;


use App\User;
use Illuminate\Console\Command;

class SlugCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slug:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get title and convert here to slug in all articles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        $this->alert('Users');
        $bar = $this->output->createProgressBar(count($users));
        foreach ($users as $user){
            $user->slug = $user->name;
            $user->save();
            $bar->advance();
        }
        $bar->finish();
    }
}
