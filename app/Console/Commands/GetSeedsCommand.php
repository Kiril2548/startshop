<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class GetSeedsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:seeds {db?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create seeds for the whole database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $choiceTable = ['All'];
        $tables = DB::select('SHOW TABLES');
            foreach ($tables as $table){
                foreach ($table as $key => $t){
                    array_push($choiceTable, $t);
                }
            }

        $value = $this->choice('For which table do the seed?', $choiceTable);

        if($value != "All"){

            $seeder = "
<?php

use Illuminate\Database\Seeder;

class ". GetSeedsCommand::camelize($t) ."Seeder extends Seeder
{
    public function run()
    {\n";

            $endseeder = "
    }
}
        ";

            $this->info("Create seed of ". $value);

            $items = DB::table($value)->get(); //Get add items from table
            $columns = Schema::getColumnListing($value); //Get all column name from table

            $bar = $this->output->createProgressBar(count($items)); //start progressbar

            foreach ($items as $item) {
                $seeder = $seeder . "       DB::table('".$value."')->insert([\n";
                foreach ($columns as $column){
                    if ($item->$column === null){} else {
                        $seeder = $seeder . "           '". $column ."' => '" . addslashes($item->$column) . "',\n";
                    }
                }
                $seeder = $seeder . "       ]);\n";
                $bar->advance();
            }

            $seeder = $seeder . $endseeder;

            $fp = fopen('database/seeds/'.GetSeedsCommand::camelize($value).'Seeder.php', 'w');
            $writeInFile = fwrite($fp, $seeder);
            if ($writeInFile) echo '';
            else echo 'Error';
            fclose($fp);

            $bar->finish();
            $this->info("\nSeed of ".$value." created success \n");

        } else {
            $tables = DB::select('SHOW TABLES');
            foreach ($tables as $table){
                foreach ($table as $key => $t){
                    $seeder = "
<?php

use Illuminate\Database\Seeder;

class ". GetSeedsCommand::camelize($t) ."Seeder extends Seeder
{
    public function run()
    {\n";

                    $endseeder = "
    }
}
        ";

                    $this->info("Create seed of ". $t);

                    $items = DB::table($t)->get(); //Get add items from table
                    $columns = Schema::getColumnListing($t); //Get all column name from table

                    $bar = $this->output->createProgressBar(count($items)); //start progressbar

                    foreach ($items as $item) {
                        $seeder = $seeder . "       DB::table('".$t."')->insert([\n";
                        foreach ($columns as $column){
                            //$this->error($item->$column);
                            if ($item->$column === null){} else {
                                $seeder = $seeder . "           '". $column ."' => '" . addslashes($item->$column) . "',\n";
                            }
                        }
                        $seeder = $seeder . "       ]);\n";
                        $bar->advance();
                    }

                    $seeder = $seeder . $endseeder;

                    $fp = fopen('database/seeds/'.GetSeedsCommand::camelize($t).'Seeder.php', 'w');
                    $writeInFile = fwrite($fp, $seeder);
                    if ($writeInFile) echo '';
                    else echo 'Error';
                    fclose($fp);

                    $bar->finish();
                    $this->info("\nSeed of ".$t." created success \n");
                }
            }
        }
    }

    public function camelize($input, $separator = '_')
    {
        return str_replace($separator, '', ucwords($input, $separator));
    }
}
