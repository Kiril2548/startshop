<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function shop(Request $request)
    {

        return view('frontend.public.shop.index_with_card');
    }

    public function shop_(Request $request)
    {

        return view('frontend.public.shop.index_with_out_card');
    }

    public function shop__(Request $request)
    {
        $categories = Category::all();
        $main_products = Product::with('category', 'imgs', 'discount')->orderBy('updated_at', 'desc')->paginate(9);


        return view('frontend.public.shop.index_catalog', compact('main_products', 'categories'));
    }
}
