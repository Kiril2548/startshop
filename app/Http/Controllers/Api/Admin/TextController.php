<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\TextRequest;
use App\Models\Text;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TextController extends Controller
{
    public function store(TextRequest $request)
    {

        $text = new Text();
        $text->fill($request->all());
        $text->save();

        return Text::all();
    }

    public function update(Text $text, TextRequest $request)
    {
        $text->fill($request->all());
        $text->save();

        return $text;
    }

    public function search(Request $request)
    {
        return Text::where('text', 'like', '%' . $request->search . '%')
            ->get();
    }

    public function destroy(Text $text)
    {
        $text->delete();
        return Text::all();
    }
}
