<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function store(CategoryRequest $request)
    {

        $category = new Category();
        $category->slug = $request->title;
        $category->fill($request->all());
        $category->save();

        return Category::with('category')->orderBy('place', 'asc')->get();
    }

    public function update(Category $category, CategoryRequest $request)
    {
        $category->fill($request->all());
        $category->slug = $category->title;
        $category->save();

        return $category;
    }

    public function destroy(Category $category)
    {
        if ($category->status == 1){
            $category->status = 0;
        } else {
            $category->status = 1;
        }
        $category->save();
        return Category::with('category')->orderBy('place', 'asc')->get();
    }

    public function search(Request $request)
    {
        return Category::where('title', 'like', '%' . $request->search . '%')->with('category')->get();
    }

    public function place(Category $category, Request $request)
    {
        $category->place = $request->palce;
        $category->save();

        return Category::orderBy('place', 'asc')->with('category')->where('status', 1)->get();
    }
}
