<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\UserUpdate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function update(User $user ,UserUpdate $request)
    {
        $user->fill($request->all());
        $user->save();

        return User::with('roles')->get();
    }

    public function search(Request $request)
    {
        return User::where('name', 'like', '%' . $request->search . '%')->orWhere('email', 'like', '%' . $request->search . '%')->with('roles')->get();
    }
}
