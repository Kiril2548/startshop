<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    public function index()
    {
        $news = News::orderBy('created_at', 'desc')->paginate(5);

        return view('frontend.public.news.index', compact('news'));
    }

    public function mini()
    {
        $news = News::orderBy('created_at', 'desc')->paginate(10);

        return view('frontend.public.news.mini', compact('news'));
    }

    public function mini_basic()
    {
        $news = News::orderBy('created_at', 'desc')->paginate(10);

        return view('frontend.public.news.mini_basic', compact('news'));
    }

    public function show(News $news)
    {
        $news->views++;
        $news->save();

        $news_all = News::orderBy('created_at', 'desc')->get();

        return view('frontend.public.news.show', compact('news', 'news_all'));

    }

}
