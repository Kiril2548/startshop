<?php

namespace App\Http\Controllers\Admin;

use App\Models\ArticleCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleCategoryController extends Controller
{

    public function index()
    {
        $categories = ArticleCategory::with('category')->orderBy('place', 'asc')->get();

        return view('admin.article_category.index', compact('categories'));
    }

    public function place()
    {
        $categories = ArticleCategory::orderBy('place', 'asc')->with('category')->where('status', 1)->get();

        return view('admin.article_category.place', compact('categories'));
    }

    public function edit(ArticleCategory $articleCategory)
    {

        $category = $articleCategory->load('category');
        $categories = ArticleCategory::all();

        return view('admin.article_category.edit', compact('categories', 'category'));
    }

}
