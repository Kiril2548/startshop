<?php

namespace App\Http\Controllers\Admin;

use App\Models\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SessionController extends Controller
{
    public function index()
    {
        $sessions = Session::with('user')->get();

        return view('admin.session.index', compact('sessions'));
    }
}
