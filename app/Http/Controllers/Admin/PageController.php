<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

    public function index()
    {
        $pages = Page::orderBy('created_at', 'desc')->get();

        foreach ($pages as $key => $page){
            $pages[$key]->key_words = json_decode($pages[$key]->key_words);
        }

        return view('admin.page.index', compact('pages'));
    }

    public function edit(Page $page)
    {
        $page->key_words = json_decode($page->key_words);

        return view('admin.page.edit', compact('page'));
    }
    
}
