<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::with('category')->orderBy('place', 'asc')->get();

        return view('admin.category.index', compact('categories'));
    }

    public function place()
    {
        $categories = Category::orderBy('place', 'asc')->with('category')->where('status', 1)->get();

        return view('admin.category.place', compact('categories'));
    }

    public function edit(Category $category)
    {
        $category = $category->load('category');
        $categories = Category::all();

        return view('admin.category.edit', compact('categories', 'category'));
    }
}
