<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class OwnerMiddleware
{
    public function handle($request, Closure $next, $role){
        if (!$request->user()->hasRole($role)) {
            return redirect()->back(); // редирект куда угодно
        }
        return $next($request);
    }
}
