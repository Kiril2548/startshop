<?php

namespace App\Providers;

use App\Models\AdminSetting;
use App\Models\Category;
use App\Models\Page;
use App\Models\Setting;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $settings_all = Setting::all();
        $settings = [];
        foreach ($settings_all as $setting){
            $settings[$setting->key] = $setting->value;
        }
        view()->share('settings', $settings);
//
        $pages = Page::all();
        view()->share('pages', $pages);
//
        $admin_settings_all = AdminSetting::all();
        $admin_settings =[];
        foreach ($admin_settings_all as $setting){
            if($setting->value == 1)
                $setting->value = true;
            else
                $setting->value = false;
            $admin_settings[$setting->key] = $setting->value;
        }
        view()->share('admin_settings', $admin_settings);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
