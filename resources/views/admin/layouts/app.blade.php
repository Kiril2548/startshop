<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Админка</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('admin.ico') }}"/>

    <link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap/dist/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css"/>
    <link rel="stylesheet" href="https://unpkg.com/buefy/dist/buefy.min.css">
    <link rel="stylesheet" href="//cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/admin_nav.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @routes
</head>
<body>
@stack('styles')
<div id="header">
    @include('admin.particles.header')
</div>

<div id="nav">
    @include('admin.particles.nav')
</div>

<div id="content">
    @yield('content')
</div>

<div id="footer">
    @include('admin.particles.footer')
</div>

<script src="https://unpkg.com/vue"></script>

<!-- Full bundle -->
<script src="https://unpkg.com/buefy/dist/buefy.min.js"></script>

<!-- Individual components -->
<script src="https://unpkg.com/buefy/dist/components/table"></script>
<script src="https://unpkg.com/buefy/dist/components/input"></script>

<!-- AXIOS -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    axios.defaults.headers.common['X-CSRF-TOKEN'] = @json(csrf_token());
</script>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="{{ asset('js/admin_nav.js') }}"></script>

@stack('scripts')

<script>
    var header = new Vue({
        el: '#header',
        data: {
            m: "HELLO"
        }
    });

    var footer = new Vue({
        el: '#footer',
        data: {
            m: "HELLO"
        }
    });
</script>


</body>
</html>
