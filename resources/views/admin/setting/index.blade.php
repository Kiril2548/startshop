@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="notification row">
            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs" style="display: flex; justify-content: space-between;">
                    <ul style="margin-top: 5px;">
                        <li><a href="{{ route('admin.main') }}">Админка</a></li>
                        <li><a href="{{ route('admin.settings.index') }}">Настройки</a></li>
                    </ul>
                    <div>
                        <button class="button is-success is-smal" @click="onUpdate()">
                            Сохранить настройки
                        </button>
                        <button class="button is-primary is-smal"
                                @click="isCardModalActive = true">
                            Добавить настройку
                        </button>
                    </div>
                    <b-modal :active.sync="isCardModalActive" :width="640" scroll="keep">
                        <div class="card">
                            <div class="card-content">
                                <div class="content">
                                    @include('admin.setting._form')
                                </div>
                            </div>
                        </div>
                    </b-modal>
                </nav>
            </div>

            <div class="card col-md-3" style="padding: 10px;" v-for="sc in settings_category">
                <header class="card-header" style="padding: 0; padding-bottom: 5px; padding-top: 5px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12" style="text-align: center;">
                                <h3><b>@{{ sc }}</b></h3>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12" v-for="setting in settings_list">
                            <b-field v-if="setting.setting_category == sc" :label="setting.key">
                                <b-input v-model="setting.value"></b-input>
                            </b-field>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .card{
            transform: scale(0.96);
        }
        .card:hover{
            box-shadow: 1px 1px 5px #333;
        }
    </style>
@endpush

@push('scripts')
    <script>
        new Vue({
            el: "#content",
            data: {
                setting: {},
                isCardModalActive: false,
                settings_list: @json($settings),
                settings_category: @json($settings_category),
            },
            methods: {
                onSave() {
                    let self = this;
                    axios.post(route('admin.settings.store'), self.setting)
                        .then(function (response) {
                            self.setting.key = '';
                            self.setting.value = '';
                            self.setting.more = '';
                            self.settings_category = response.data['settings_category'];
                            self.settings_list = response.data['settings'];
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Настройка успешно добавленна`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                },
                onUpdate() {
                    let self = this;
                    axios.post(route('admin.settings.update'), self.settings_list)
                        .then(function (response) {
                            self.settings_category = response.data['settings_category'];
                            self.settings_list = response.data['settings'];
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Настройка успешно сохранены`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
@endpush
