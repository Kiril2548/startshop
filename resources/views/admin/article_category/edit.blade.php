@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="notification row">
            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="{{ route('admin.main') }}">Админка</a></li>
                        <li><a href="{{ route('admin.article-categories.index') }}">Категории (Статьи)</a></li>
                        <li><a href="#">Редактирование категории :<b>{{ $category->title }}</b></a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card" style="padding: 15px;">@include('admin.category._form')</div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        var slug = @json($category->slug);
        new Vue({
            el: "#content",
            data: {
                errors: {},
                category: @json($category),
                categories: @json($categories)
            },
            methods: {
                onSave() {
                    let self = this;
                    axios.put(route('admin.article-categories.update', self.category.slug), self.category)
                        .then(function (response) {
                            self.errors = {};
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Категория успешно обновлена`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                            setTimeout(function () {
                                if(response.data.slug != slug){
                                    console.log(response.data.slug + ' -> ' + slug);
                                    window.location = route('admin.article-categories.edit', response.data.slug);
                                }
                            }, 1500);
                        })
                        .catch(function (error) {
                            self.errors= {};
                            let response = error.response;
                            if(response.status === 422) {

                                self.errors = response.data.errors;
                                Object.keys(response.data.errors).map((key) => {
                                    self.errors[key] = response.data.errors[key][0];
                                });
                            }
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
@endpush
