@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="notification row">
            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="{{ route('admin.main') }}">Админка</a></li>
                        <li><a href="{{ route('admin.pages.index') }}">Публичные страницы</a></li>
                        <li><a href="#">Редактирование страницы: {{ $page->title }}</a></li>
                    </ul>
                </nav>
            </div>

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card" style="padding: 15px;">@include('admin.page._form')</div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        var lfm = function (options, cb) {

            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=1280,height=600');
            window.SetUrl = cb;
        }
    </script>
    <script>
        var slug = @json($page->slug);
        new Vue({
            el: "#content",
            data: {
                errors: {},
                page: @json($page),
            },
            methods: {
                imageModal(url) {
                    this.$modal.open(
                        `<p class="image is-4by3">
                            <img style="object-fit: cover;" src="` + url + `">
                        </p>`
                    )
                },
                lfmm() {
                    let self = this;
                    var img_url = lfm({type: 'image', prefix: ''}, function (url, path) {
                        self.addImgToArray(url);
                    });
                },
                addImgToArray(url) {
                    this.page.img = url;
                },
                delImgFromArray(key) {
                    this.page.img = ""
                },
                onSave() {
                    let self = this;
                    axios.put(route('admin.pages.update', self.page.slug), self.page)
                        .then(function (response) {
                            self.errors = {};
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Страница успешно сохранена`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                            setTimeout(function () {
                                if (response.data.slug != slug) {
                                    window.location = route('admin.pages.edit', response.data.slug);
                                }
                            }, 1500);
                        })
                        .catch(function (error) {
                            let response = error.response;
                            if(response.status === 422) {

                                self.errors = response.data.errors;
                                Object.keys(response.data.errors).map((key) => {
                                    self.errors[key] = response.data.errors[key][0];
                                });
                            }
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
@endpush
