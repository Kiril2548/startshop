@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="notification row">
            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="{{ route('admin.main') }}">Админка</a></li>
                        <li><a href="{{ route('admin.news.index') }}">Новости</a></li>
                        <li><a href="#">Редактирование новости: {{ $news->title }}</a></li>
                    </ul>
                </nav>
            </div>

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card" style="padding: 15px;">@include('admin.new._form')</div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        var lfm = function (options, cb) {

            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=1280,height=600');
            window.SetUrl = cb;
        }
    </script>
    <script>
        var slug = @json($news->slug);
        new Vue({
            el: "#content",
            data: {
                errors: {},
                news: @json($news),
            },
            methods: {
                imageModal(url) {
                    this.$modal.open(
                        `<p class="image is-4by3">
                            <img style="object-fit: cover;" src="` + url + `">
                        </p>`
                    )
                },
                lfmm() {
                    let self = this;
                    var img_url = lfm({type: 'image', prefix: ''}, function (url, path) {
                        self.addImgToArray(url);
                    });
                },
                addImgToArray(url) {
                    this.news.img = url;
                },
                delImgFromArray(key) {
                    this.news.img = ""
                },
                onSave() {
                    let self = this;
                    axios.put(route('admin.news.update', self.news.slug), self.news)
                        .then(function (response) {
                            self.errors = {};
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Страница успешно сохранена`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                            setTimeout(function () {
                                if (response.data.slug != slug) {
                                    window.location = route('admin.news.edit', response.data.slug);
                                }
                            }, 1500);
                        })
                        .catch(function (error) {
                            let response = error.response;
                            if(response.status === 422) {

                                self.errors = response.data.errors;
                                Object.keys(response.data.errors).map((key) => {
                                    self.errors[key] = response.data.errors[key][0];
                                });
                            }
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
@endpush
