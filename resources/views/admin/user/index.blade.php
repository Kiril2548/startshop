@extends('admin.layouts.app')

@push('styles')
    <style>
        .img-preview-table:hover {
            cursor: pointer;
        }
    </style>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="notification row">

            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="{{ route('admin.main') }}">Админка</a></li>
                        <li><a href="{{ route('admin.users.index') }}">Пользователи</a></li>
                    </ul>
                </nav>
            </div>

            <div class="card col-md-12" style="padding: 5px;">
                <header class="card-header" style="padding: 0; padding-bottom: 5px; padding-top: 5px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <b-field
                                    label="Позицый на страницу">
                                    <b-select v-model="per_page" expanded>
                                        <option v-for="item in pre_page_a" :value="item">@{{ item }}</option>
                                    </b-select>
                                </b-field>
                            </div>
                            <div class="col-md-4">
                                <b-field
                                    label="Поиск">
                                    <div class="field has-addons">
                                        <div class="control" style="width: 100%;">
                                            <input class="input" type="text" v-model="filter" placeholder="Поиск">
                                        </div>
                                        <div class="control">
                                            <a class="button is-primary is-outlined" @click="onSearch(filter)">
                                                Найти
                                            </a>
                                        </div>
                                    </div>
                                </b-field>
                            </div>
                            <div class="col-md-4">
                                {{--<button class="button is-primary is-smal"--}}
                                        {{--style="float: right; margin-bottom: 10px; margin-top: 15px;"--}}
                                        {{--@click="isCardModalActive = true">--}}
                                    {{--Добавить товар--}}
                                {{--</button>--}}
                                <b-modal :active.sync="isCardModalActive" :width="640" scroll="keep">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="content">
                                                @include('admin.user._form')
                                            </div>
                                        </div>
                                    </div>
                                </b-modal>
                            </div>
                        </div>
                    </div>
                </header>
                <b-table
                    :data="data"
                    paginated
                    hoverable
                    mobile-cards
                    :filter="filter"
                    :per-page="per_page"
                    :opened-detailed="defaultOpenedDetails"
                    detailed
                    detail-key="id"
                    @details-open="(row, index) => $toast.open(`Доп.Информация ${row.name} открыта`)">
                    <template slot-scope="props">

                        <b-table-column field="id" label="ID" width="40" sortable numeric>
                            @{{ props.row.id }}
                        </b-table-column>

                        <b-table-column field="img" label="Аватар" width="60" sortable numeric>
                            <img @click="imageModal(props.row.avatar)" class="img-preview-table"
                                 :src="props.row.avatar" alt="" style="height: 30px; width: 45px; object-fit: cover;">
                        </b-table-column>

                        <b-table-column field="name" label="Имя" sortable>
                            @{{ props.row.name }}
                        </b-table-column>

                        <b-table-column field="email" label="Почта" sortable>
                            @{{ props.row.email }}
                        </b-table-column>

                        <b-table-column field="created_at" label="Дата регистрации" sortable>
                            @{{ props.row.created_at }}
                        </b-table-column>


                        <b-table-column field="action" label="" sortable numeric>
                            <div class="btn-group">
                                <a :href="routeEdit+ '/' + props.row.id + '/edit'"
                                   class="btn btn-outline-primary btn-sm">EDIT</a>
                                {{--<button @click="onDelete(props.row.id)" class="btn btn-outline-danger btn-sm">DELETE--}}
                                {{--</button>--}}
                            </div>
                        </b-table-column>
                    </template>
                    <template slot="empty">
                        <section class="section">
                            <div class="content has-text-grey has-text-centered">
                                <p>
                                    <b-icon
                                        icon="emoticon-sad"
                                        size="is-large">
                                    </b-icon>
                                </p>
                                <p>Ничего нет.</p>
                            </div>
                        </section>
                    </template>
                    <template slot="detail" slot-scope="props">
                        <article class="media">
                            <div class="media-content">
                                <div class="content">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>
                                                    <img
                                                         style="width: 240px; object-fit: cover"
                                                         :src="props.row.avatar">
                                                    <br>
                                                    <br>
                                                    <strong>@{{ props.row.name }}</strong> <sup>@{{
                                                        props.row.roles[0].name }}</sup>
                                                    <br>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </template>
                </b-table>

            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        var lfm = function (options, cb) {

            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=1440,height=960');
            window.SetUrl = cb;
        }
    </script>
    <script>
        var vue = new Vue({
            el: "#content",
            data: {
                routeEdit: route('admin.users.index'),
                filter: '',
                per_page: 10,
                pre_page_a: [5, 10, 20, 50, 100],
                user: {},
                defaultOpenedDetails: [],
                isCardModalActive: false,
                data: @json($users),
                roles: @json($roles),
                errors: {}
            },
            methods: {
                imageModal(url) {
                    this.$modal.open(
                        `<p class="image is-4by3">
                            <img style="object-fit: cover;" src="` + url + `">
                        </p>`
                    )
                },
                lfmm() {
                    let self = this;
                    var img_url = lfm({type: 'image', prefix: ''}, function (url, path) {
                        self.addImgToArray(url);
                    });
                },
                addImgToArray(url) {
                    this.imgs.push({url: url});
                },
                delImgFromArray(key) {
                    this.imgs.splice(key, 1)
                },
                onDelete(index) {
                    let self = this;
                    axios.delete(route('admin.users.destroy', index))
                        .then(function (response) {
                            self.data = response.data;
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Статус продукта успешно изменен`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                },
                onSave() {
                    let self = this;
                    axios.post(route('admin.users.store'), this.user)
                        .then(function (response) {
                            self.data = response.data;
                            self.user= {};
                            self.errors = {};
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Продукт успешно сохранена`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.errors = {};
                            let response = error.response;
                            if(response.status === 422) {

                                self.errors = response.data.errors;
                                Object.keys(response.data.errors).map((key) => {
                                    self.errors[key] = response.data.errors[key][0];
                                });
                            }
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                },
                onSearch(search) {
                    let self = this;
                    axios.post(route('admin.users.search'), {
                        search: search
                    })
                        .then(function (response) {
                            self.data = response.data;
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Поиск прошел успешно`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время поиска произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
@endpush
