@extends('frontend.layouts.app')

@section('content')

    <div class="container mt-3">
        <div class="row">
            @foreach($news as $new)
                <div class="col-md-12 mb-4">
                    <div class="card">
                        <div class="img p-3">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-3" style="
                                        background-image: url({{ $new->img }});
                                        background-size: cover;
                                        background-position: center;
                                        "></div>
                                    <div class="col-md-9">
                                        <h3 class="font-weight-bold"><a
                                                href="{{ route('news.show', $new->slug) }}">{{ $new->title }}</a></h3>
                                        <p>{!! substr($new->text, 0, 500) !!}...<a
                                                style="font-weight: 600; text-decoration: none;"
                                                href="{{ route('news.show', $new->slug) }}">Перейти
                                                <i class="fas fa-arrow-circle-right"></i></a></p>
                                        <div class="d-flex justify-content-between pl-3 pr-3">
                                            <span><i class="fas fa-eye"></i> {{ $new->views }}</span>
                                            <span><i class="fas fa-clock"></i> {{ $new->created_at }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="w-100 d-flex justify-content-center">
                {{ $news->links() }}
            </div>
        </div>
    </div>

@endsection

