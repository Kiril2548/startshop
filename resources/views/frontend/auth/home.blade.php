@extends('frontend.layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    You are logged in!
                        <hr>
                    @{{ m }} [ {{ Auth::user()->name }} ]
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
    <script>
        new Vue({
            el: "#app",
            data: {
                m: "HELLO"
            }
        });
    </script>
@endpush
