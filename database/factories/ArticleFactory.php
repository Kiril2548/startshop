<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Article::class, function (Faker $faker) {
    return [
        'slug' => $faker->slug,
        'article_category_id' => rand(1,5),
        'title' => $faker->company,
        'img'   => 'https://picsum.photos/1980/1080/?random',
        'text' => $faker->text(10000)
    ];
});
