<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('payment_id'); // не связь !!!!

            $table->string('order_id')->nullable();

            $table->unsignedInteger('user_id')->nullable();
            $table->string('who')->default('user');

            $table->string('status');

            $table->string('payment_system')->nullable();

            $table->double('amount', 8, 2);

            $table->double('balance', 8, 2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
