
<?php

use Illuminate\Database\Seeder;

class SessionsSeeder extends Seeder
{
    public function run()
    {
       DB::table('sessions')->insert([
           'id' => '9ELTbvP53v8qqfZBW2bbdBRdTbR8iKpupbjcUsaO',
           'user_id' => '1',
           'ip_address' => '127.0.0.1',
           'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36 OPR/56.0.3051.104',
           'payload' => 'YTo0OntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiVFIwa25hRmd4UU9HcmxSNEZuYjZ2clU5OGxiWEI0TVIxckgxdXBOMyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NzQ6Imh0dHA6Ly9zdGFydC5sb2MvYXJ0aWNsZXMvZXJyb3ItZWEtZXN0LWV1bS1lc3QtbGFib3JlLXZpdGFlLXBhcmlhdHVyLWVycm9yIjt9czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTt9',
           'last_activity' => '1543210127',
       ]);

    }
}
        