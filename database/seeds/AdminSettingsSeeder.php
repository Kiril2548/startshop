
<?php

use Illuminate\Database\Seeder;

class AdminSettingsSeeder extends Seeder
{
    public function run()
    {
       DB::table('admin_settings')->insert([
           'id' => '17',
           'key' => 'e_commerce_d',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '18',
           'key' => 'blog_d',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '19',
           'key' => 'session',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '20',
           'key' => 'category',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '21',
           'key' => 'products',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '22',
           'key' => 'orders',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '23',
           'key' => 'payments',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '24',
           'key' => 'carts',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '25',
           'key' => 'pages',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '26',
           'key' => 'news',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '27',
           'key' => 'articles',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '28',
           'key' => 'texts',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '29',
           'key' => 'settings',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '30',
           'key' => 'api_settings',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '31',
           'key' => 'roles',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '32',
           'key' => 'permissions',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);
       DB::table('admin_settings')->insert([
           'id' => '33',
           'key' => 'article_categories',
           'value' => '1',
           'updated_at' => '2018-11-24 09:35:26',
       ]);

    }
}
        