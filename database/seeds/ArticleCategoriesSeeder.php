
<?php

use Illuminate\Database\Seeder;

class ArticleCategoriesSeeder extends Seeder
{
    public function run()
    {
       DB::table('article_categories')->insert([
           'id' => '1',
           'title' => 'Hansen-Kessler',
           'slug' => 'et-et-explicabo-sequi-molestiae-tempore',
           'place' => '1',
           'status' => '0',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('article_categories')->insert([
           'id' => '2',
           'title' => 'Raynor, Gerhold and Kuhlman',
           'slug' => 'neque-id-similique-qui-consequatur',
           'place' => '1',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('article_categories')->insert([
           'id' => '3',
           'title' => 'Runolfsson, Walsh and Kozey',
           'slug' => 'quia-eos-impedit-nulla-officiis',
           'place' => '1',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('article_categories')->insert([
           'id' => '4',
           'title' => 'Kemmer LLC',
           'slug' => 'rerum-ratione-velit-sequi-aut-dolorem-in-id',
           'place' => '1',
           'status' => '0',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('article_categories')->insert([
           'id' => '5',
           'title' => 'Daniel and Sons',
           'slug' => 'eos-perspiciatis-repudiandae-optio-deleniti-molestiae-repellat-eos',
           'place' => '1',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);

    }
}
        