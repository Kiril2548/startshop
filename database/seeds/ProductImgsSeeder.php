
<?php

use Illuminate\Database\Seeder;

class ProductImgsSeeder extends Seeder
{
    public function run()
    {
       DB::table('product_imgs')->insert([
           'id' => '1',
           'product_id' => '1',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '2',
           'product_id' => '2',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '3',
           'product_id' => '3',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '4',
           'product_id' => '4',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '5',
           'product_id' => '5',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '6',
           'product_id' => '6',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '7',
           'product_id' => '7',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '8',
           'product_id' => '8',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '9',
           'product_id' => '9',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '10',
           'product_id' => '10',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '11',
           'product_id' => '11',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '12',
           'product_id' => '12',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '13',
           'product_id' => '13',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '14',
           'product_id' => '14',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '15',
           'product_id' => '15',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '16',
           'product_id' => '16',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '17',
           'product_id' => '17',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '18',
           'product_id' => '18',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '19',
           'product_id' => '19',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '20',
           'product_id' => '20',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '21',
           'product_id' => '21',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '22',
           'product_id' => '22',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '23',
           'product_id' => '23',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '24',
           'product_id' => '24',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('product_imgs')->insert([
           'id' => '25',
           'product_id' => '25',
           'url' => 'https://picsum.photos/1980/1080/?random',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);

    }
}
        