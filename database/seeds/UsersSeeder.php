
<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
       DB::table('users')->insert([
           'id' => '1',
           'amount_for_ref' => '0',
           'slug' => '1543203343',
           'avatar' => 'https://picsum.photos/1980/1080/?random',
           'name' => 'test11@test11.test11',
           'email' => 'test11@test11.test11',
           'balance' => '0',
           'baned' => '0',
           'password' => '$2y$10$gN75ra9ormo/xUt1MCfQLOk/L6dFNN3IqniRneMBQpyYxDtZ16baW',
           'remember_token' => 'k3JuASDNMNDZSmPkIZlKxH3gGzblDxjtZMQbsGbdhLoWx3gF37bHRpiWdlke',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('users')->insert([
           'id' => '2',
           'amount_for_ref' => '0',
           'slug' => '1543203343',
           'avatar' => '/unknow_user.png',
           'name' => 'test12@test11.test11',
           'email' => 'test12@test11.test11',
           'balance' => '0',
           'baned' => '0',
           'password' => '$2y$10$FwO2C.E6LjB9DT2QVFA7PuDE1XZbvLunbKqRgJWVgaims7sm76zoi',
           'remember_token' => 'nYqiZtbVjXpdgH8QL6ESfv0q5DnovgAIEFRBfTTxHmgHR0SJYK8iLN1JixfU',
           'created_at' => '2018-11-26 05:23:29',
           'updated_at' => '2018-11-26 05:23:29',
       ]);

    }
}
        