
<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    public function run()
    {
       DB::table('products')->insert([
           'id' => '1',
           'slug' => 'inventore-rerum-aperiam-sint-repellat-ad',
           'category_id' => '5',
           'discount_id' => '36',
           'title' => 'Rau-Huel',
           'desc' => 'Autem maiores non minima sed. Quas repellendus ducimus rerum dolorem atque. Ipsa provident eligendi minus nesciunt aut commodi nemo. Occaecati libero quis dolorem ipsum ut. Aliquam veritatis quibusdam dolorem ea. Omnis commodi dicta voluptas voluptates odit. Fuga expedita ut eligendi reiciendis quam consequatur. Eos tempore vitae et nemo. Aut repellat cumque nisi rem. Natus numquam quaerat ab similique mollitia ipsum qui. Atque distinctio similique officiis dolorum numquam eaque. Qui provident ut commodi et architecto. Expedita accusantium adipisci aperiam illum. Molestiae ipsam recusandae aut asperiores rerum magni unde. Laboriosam non odit totam id explicabo. Autem commodi omnis molestiae magni aut rerum ea. Est quam alias dolore vitae. Est et exercitationem modi facilis perspiciatis molestiae. Id beatae aut ut unde nam. Illum eos in velit tenetur aspernatur. Ab totam labore facilis eveniet deserunt vitae. Et error laborum non. Esse omnis distinctio sint tempora sunt veniam quaerat.',
           'price' => '136.74',
           'count' => '18',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '2',
           'slug' => 'harum-ullam-aliquam-nisi',
           'category_id' => '4',
           'discount_id' => '62',
           'title' => 'Rau, Hermiston and Morissette',
           'desc' => 'Aperiam quasi voluptatum cum. Odio minus vel architecto in. Deserunt debitis quia recusandae id quo. Corrupti veniam voluptas inventore unde voluptas optio perferendis. Voluptatem maiores quaerat minima omnis perspiciatis unde. Qui dicta a inventore a qui. Enim iusto id sed aut ut quia. Dolore natus consequatur eius sint. Impedit aut eos temporibus vitae et. Voluptatum dolor inventore ad aut vero autem eos voluptatem. Maiores neque dolore voluptas voluptatibus libero deleniti error. Dignissimos distinctio iusto esse quia quis et. Hic nihil tempore ea facere voluptate autem. Iusto aut nihil aspernatur odio assumenda officia. Et quos quis vel voluptas voluptas. Sit dolorem quidem blanditiis maxime eveniet fugit. Deleniti molestiae at aut enim unde vero. Qui dignissimos quidem et non repellat error. Voluptatem enim minus assumenda aut eos sed.',
           'price' => '139.78',
           'count' => '19',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '3',
           'slug' => 'est-autem-dolores-dolorum-nemo-debitis-ex',
           'category_id' => '1',
           'discount_id' => '17',
           'title' => 'Lang and Sons',
           'desc' => 'Numquam odit incidunt repellendus sint sit inventore. Sequi fugit perspiciatis accusamus corporis non. Sunt magnam dicta amet dicta. Veritatis nisi omnis temporibus placeat. Harum quisquam temporibus velit fugit sunt. Tempore rerum repellat occaecati ipsa quibusdam. Sed tempore nihil eos non qui. Iste eius suscipit error. Et enim ut voluptatem incidunt est harum. Vero iusto ducimus dolor quod. Reiciendis unde quidem est corporis sed non. Officia deleniti sit mollitia. Est nesciunt illum minima debitis perferendis autem ut. Sed quis voluptatem eos delectus quis. Et temporibus voluptate fugiat illo velit. Ut quo eum beatae tempora perspiciatis vero ut. Fugiat expedita cum consequatur sapiente eveniet. Quibusdam sint est labore cum qui adipisci. Aut sit a dolorem fugit ullam. Et quos rerum ex omnis ipsam sed consequatur. Rerum dolorem est magnam vel quas dolores itaque rerum.',
           'price' => '145.04',
           'count' => '83',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '4',
           'slug' => 'enim-nostrum-sunt-et-impedit-quidem-adipisci-dignissimos',
           'category_id' => '5',
           'discount_id' => '96',
           'title' => 'Hessel, Nienow and Streich',
           'desc' => 'Quis fugiat quae qui quia odio dignissimos. Ut voluptatum facere porro provident ea enim ratione. Doloribus iure voluptas consequatur ut. Qui dolore tempore nulla dolores praesentium sed veritatis. Quia nulla aut amet nihil officiis. Et enim dolores voluptatem sit. Atque dicta ex quas maxime voluptatibus aut et quis. Sed ut qui velit adipisci. Assumenda sint repellat velit sequi et quos dolor. Tenetur et voluptatibus sint dolorum libero consequatur exercitationem. Quia fugiat architecto dolor voluptas illum sint minima voluptatem. Eos maxime fuga sed eaque ullam blanditiis enim. Tempore dolore temporibus iure iusto animi. Officiis molestiae inventore suscipit et. Ab dolore mollitia adipisci fugit aut ut qui. Tempora quas sapiente accusantium sed veritatis. Dolorum magni aperiam qui nemo quo aliquid molestiae.',
           'price' => '50.45',
           'count' => '31',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '5',
           'slug' => 'quas-atque-est-dignissimos-hic-at-voluptas',
           'category_id' => '5',
           'discount_id' => '47',
           'title' => 'Russel, Little and Kihn',
           'desc' => 'Aut ullam nemo maiores. At aut aut modi voluptatum. Numquam temporibus rerum maxime eos vel. Consequatur accusamus consequatur temporibus. Mollitia quam praesentium enim dolores neque consequatur repellendus. Libero facilis quos vel. Aliquid et laboriosam ad eos. Officiis dolorem qui porro quis occaecati voluptatem a quo. Placeat voluptas ullam quo consequuntur. Minima quas et reprehenderit sapiente molestiae et. Ut illum et quaerat consequatur autem voluptas totam error. Possimus est ipsam saepe et autem et repellat. Quidem magni minima quidem ex rem consequuntur. Eveniet fugit odit ea. Tenetur tempora necessitatibus ratione incidunt sint ut. Eos autem deserunt sapiente voluptatem eum consequatur laborum qui. Dolorem atque alias sed debitis. Hic aliquam sit qui dolor. Ut ullam quaerat autem eveniet. Est nulla voluptatem illum ut. Porro blanditiis repudiandae eveniet alias ipsa. Omnis tenetur consequatur nihil aut reiciendis. Est adipisci at expedita alias.',
           'price' => '153.08',
           'count' => '56',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '6',
           'slug' => 'esse-aut-est-facere-fuga-eaque-in',
           'category_id' => '5',
           'discount_id' => '32',
           'title' => 'Davis, Effertz and Turcotte',
           'desc' => 'Sunt sit dolorum impedit voluptates. Eius corrupti et consequatur consequuntur sit. Consequatur quisquam et eligendi distinctio id rem. Maiores quis qui eos ea non sit. Et nihil iure quia autem qui quibusdam. Aliquam labore delectus tenetur cum. Deleniti odio modi ex dicta laudantium inventore. Aut aut dolores deserunt tempore aliquam molestiae possimus laborum. Voluptatem voluptas ab accusamus sed quae tempora excepturi. Ratione minima autem odit sed omnis. Molestiae corrupti et dignissimos vel nulla qui voluptatem aperiam. Non libero voluptatem voluptas et vel aut odio. Beatae ipsam porro ipsam reiciendis a qui similique. Porro aut corporis ut adipisci aspernatur sequi molestiae. Exercitationem et non est ducimus. Ex qui est odit dolorem est sint est. Ex dicta fugiat voluptas illum eos quasi voluptas voluptas. Eos earum rerum accusamus et reiciendis inventore. Quo rerum est nulla ut similique.',
           'price' => '34.39',
           'count' => '68',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '7',
           'slug' => 'dolores-eum-commodi-sed-voluptate-vel-sit',
           'category_id' => '3',
           'discount_id' => '46',
           'title' => 'Towne, Fritsch and Veum',
           'desc' => 'Voluptates amet recusandae modi quis. Quas inventore quia corrupti. Labore saepe laborum maxime maiores. Quo suscipit quas exercitationem minima exercitationem deserunt tempore aut. Ipsum ab iure quo minus. Voluptas nobis vel quae deleniti. Qui ut ea dolorem voluptatem sit alias aspernatur aut. Magni aperiam dolorem mollitia consequuntur repellendus id. Et iste rerum voluptatem culpa inventore repudiandae minima illum. Id perspiciatis facilis voluptatum similique. Et saepe possimus rerum vero hic amet. Autem corrupti necessitatibus eum ea. Debitis nobis iusto ipsum nemo qui. Recusandae architecto in eaque omnis neque et ut. Voluptate expedita suscipit qui velit distinctio labore. Cum laudantium dolores quis sint est. Numquam rem incidunt cumque non. Labore et temporibus qui. Alias quia dolor rerum. Sit adipisci fugit voluptates deleniti. Qui veniam exercitationem non et ipsa ex consectetur. Ducimus deleniti et distinctio qui natus.',
           'price' => '121.84',
           'count' => '10',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '8',
           'slug' => 'maxime-et-inventore-doloremque-debitis',
           'category_id' => '2',
           'discount_id' => '83',
           'title' => 'Reilly Ltd',
           'desc' => 'Aut aut id est quia ab et rem. Assumenda aspernatur accusantium sint enim quaerat. Iure amet consectetur omnis. Rem ut pariatur nemo id. Expedita culpa fugiat expedita accusantium est. Nam nihil cumque eveniet perferendis atque. Sit qui incidunt dolores cupiditate qui consequatur. Et enim provident eum ea facere et omnis. Nobis voluptatem fugiat sint incidunt et dolor. Dolor atque voluptates velit culpa. Dolor saepe facilis veniam voluptas quia id. Harum quia et esse totam voluptatem. In eaque et voluptate doloribus ducimus. Vel natus ullam eum est. Molestias velit voluptas quia eligendi in numquam excepturi. Libero et quia eos officiis culpa sed laboriosam fuga. Soluta sequi et ullam dignissimos dicta officia id quisquam. Ullam officia asperiores minus unde sed expedita. Provident quam et occaecati eligendi. Quo cumque laborum debitis hic eos. Aut ut nam rerum iusto. Illum repellat sequi voluptatem dolorem et. Saepe error a excepturi est similique.',
           'price' => '307.2',
           'count' => '48',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '9',
           'slug' => 'iusto-voluptates-aliquid-est-modi-nobis',
           'category_id' => '1',
           'discount_id' => '10',
           'title' => 'Schoen-Little',
           'desc' => 'Quisquam aut voluptatibus dicta sed. Magnam velit doloribus nulla est debitis. Minus voluptas vitae dicta et aut possimus. Quis temporibus ut ad iure. Qui consequatur dignissimos est mollitia porro esse inventore. Necessitatibus cum ea placeat nemo quas voluptas quia praesentium. Autem dolor consequatur sapiente adipisci soluta. Et expedita qui natus beatae blanditiis aut consequatur omnis. Veniam itaque autem vel quibusdam debitis repellat. Vitae quidem exercitationem sint sequi nemo consectetur. Quae quisquam dolorem incidunt eos et quia. Earum voluptas impedit corporis non et officiis alias. Velit dolores in unde cupiditate autem delectus. Aut est ut placeat voluptates ea placeat. Temporibus et commodi officia dolores. Dolorem repellendus aut eligendi reprehenderit. Optio quia quo velit illo facilis quia inventore. Non molestiae sapiente iste eum velit sit ducimus. Numquam aut quia nihil enim amet. Voluptatibus enim labore eius aspernatur sit.',
           'price' => '249.09',
           'count' => '43',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '10',
           'slug' => 'omnis-doloremque-et-earum-nemo-vel-nostrum-consequuntur',
           'category_id' => '5',
           'discount_id' => '6',
           'title' => 'Crooks Group',
           'desc' => 'Ut ut quibusdam dicta provident. Repellendus impedit fugiat voluptas consequatur repellat nesciunt. Aut dolor labore autem quidem quod assumenda. Fugiat autem natus eaque et harum illum architecto officiis. Enim quas quasi nesciunt quia cum. Dolores commodi vero suscipit facilis voluptas. Nam ea harum dolores provident sit iste rem. Et est vero sed ex libero in. Accusantium repellat quaerat in commodi et. Aperiam consequatur vero qui ut. Hic eum id ad error cumque blanditiis sit. Rerum velit non alias id maxime. Earum quidem cupiditate labore. Nulla possimus est qui non velit. Eos mollitia animi facere velit. Voluptas perspiciatis saepe illum maxime laborum. Saepe ut eum id voluptatum illo vel consequuntur aliquam. Non rem ipsum rem accusamus nisi. Deserunt animi et voluptatem asperiores dolor eius. Aspernatur eveniet cumque iste. Amet quis modi non sunt iure error ipsum. Hic quo quibusdam ut magnam. Rem veritatis ut ipsa at.',
           'price' => '347.28',
           'count' => '41',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '11',
           'slug' => 'autem-enim-facilis-labore-accusamus-omnis',
           'category_id' => '5',
           'discount_id' => '93',
           'title' => 'O\'Keefe PLC',
           'desc' => 'Occaecati ipsa ad reiciendis dolorem commodi sed nulla. Eveniet quod delectus velit repudiandae quasi. Aut est at sint inventore. Rerum unde mollitia corrupti consectetur. Animi officiis ullam quis qui. Illo quae omnis distinctio ea soluta. Ut blanditiis dolorem voluptates voluptates quibusdam aut. Earum ratione optio eius ut voluptatem eum aperiam. Et aut aut asperiores sit commodi praesentium. Aut excepturi cum deserunt rem qui optio. Non cumque occaecati aut quisquam fuga est in. Sint ut ut deleniti. Quam illum voluptas est temporibus quo porro. Ex quibusdam facilis deleniti qui. Nam sunt et repellendus odio unde ratione. Qui molestiae aut quod soluta distinctio quasi. Ea et et ut nisi expedita dolores. Id ut neque ad officiis. Quia nisi ab itaque illo qui facilis quia. Consequatur ducimus aliquam ipsum accusantium possimus ut fugiat. Eveniet eum architecto aperiam animi quo dolorum et officiis.',
           'price' => '454.8',
           'count' => '26',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '12',
           'slug' => 'nesciunt-nulla-quis-quibusdam-et',
           'category_id' => '3',
           'discount_id' => '44',
           'title' => 'Stroman, Oberbrunner and Hermann',
           'desc' => 'Reprehenderit accusantium sit non nam neque autem veniam necessitatibus. Quisquam in tempora quo in molestiae. Eos voluptas non et at et perferendis sed doloremque. Voluptates quaerat similique consequatur velit rerum ut et. Id qui molestias quo sit maiores qui distinctio. Veritatis id itaque incidunt aspernatur vitae molestiae doloremque. Repellat aliquid voluptate et voluptatem minima vitae qui tempore. Distinctio architecto harum voluptatibus enim deserunt dolorem. Tempora recusandae optio tempore soluta vel voluptates. Sint optio unde iusto. Aliquid ullam autem quidem saepe aut. Sint aut consequuntur modi facilis consequuntur facere maxime. Voluptatem autem libero quidem aut. Deserunt aliquam cumque autem provident reiciendis beatae. Reprehenderit magni est voluptas quas eum. In sit porro eveniet dolores perspiciatis. Cupiditate quia eos excepturi excepturi est aut. Explicabo quam eaque aut fugit.',
           'price' => '403.53',
           'count' => '65',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '13',
           'slug' => 'fuga-voluptatem-in-beatae-vel-qui-rerum',
           'category_id' => '3',
           'discount_id' => '8',
           'title' => 'Schmitt, Torp and Huel',
           'desc' => 'Aut est et est rem. Praesentium aliquid sequi voluptas aut ut sint ab. Quia excepturi ut veritatis quo similique culpa. Ullam veritatis ex soluta et. Assumenda excepturi et ad libero omnis. Sed vero qui sint necessitatibus sed hic. Saepe adipisci optio quia aspernatur vero est. Itaque odio rem sit accusamus. Nihil voluptas et architecto fugiat. Sit laborum nisi labore officiis doloribus. Eius blanditiis quae tempore molestias maiores aspernatur beatae iusto. Et quo sunt eum id. Ut tenetur laudantium sint vitae consequuntur. Similique sed omnis reprehenderit assumenda ut neque ex est. Voluptatum possimus est qui blanditiis veniam expedita. Odit magnam sint harum quod perferendis omnis quasi. Minima voluptatem quam vel fugiat. Voluptatem minus in velit impedit. Aliquid hic voluptatem exercitationem inventore amet. Veniam velit ut in veritatis. A totam quidem sunt quia sit dicta dolor consequatur.',
           'price' => '233.01',
           'count' => '89',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '14',
           'slug' => 'corporis-officia-esse-blanditiis-est-et-rerum-et-explicabo',
           'category_id' => '4',
           'discount_id' => '39',
           'title' => 'Walker Ltd',
           'desc' => 'Doloremque autem nobis maxime deserunt minima. In fuga inventore beatae voluptatem sit. Et minima ex molestias corporis neque non porro. Cum maxime ea deleniti eos tenetur molestiae est neque. Magnam quas et sed odio. Possimus sint eum voluptatem accusamus ut ea sunt. Quas commodi repellendus harum exercitationem quo. Est sit quod at velit nisi provident voluptatem. Voluptatem provident ut autem non minus. Aut repudiandae illum quis ea nam. Dicta qui a iure omnis autem voluptas. Ut magni id modi voluptates saepe. Ut voluptas fuga tempore. Ea voluptas voluptatem natus consequatur alias fugiat. Accusamus recusandae omnis est ut ut earum. Ut beatae sint maiores non soluta autem rem. Voluptatem blanditiis molestiae saepe illum. Voluptatem voluptas excepturi omnis rerum. Magni repellat voluptatibus qui numquam illum dolorem quas. Ut eum illum eos earum. Voluptatem nulla non sed dolores recusandae unde.',
           'price' => '496.21',
           'count' => '59',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '15',
           'slug' => 'quod-minus-hic-quibusdam-distinctio',
           'category_id' => '4',
           'discount_id' => '50',
           'title' => 'Cartwright, Yost and Mayert',
           'desc' => 'Officiis assumenda fugiat ab similique. Itaque enim mollitia temporibus provident dolorem quibusdam. Maxime suscipit dolor sit voluptas. Et ullam consequatur sit consectetur libero. Quis inventore dolores natus totam. Rerum ducimus qui a est a eum. Ducimus repellendus et praesentium dicta saepe mollitia qui. Eligendi adipisci veritatis libero eum. Dolor aspernatur aut sit molestiae et. Rerum nam at ducimus quod voluptas sed eum. Architecto nemo aut non quasi molestiae laboriosam dolorum. Et et aperiam fuga deserunt voluptatem. Ab iure tempora ut nobis tempora et mollitia cum. Praesentium aperiam vel quo perferendis. Beatae corrupti et id porro voluptate at maiores. Et eligendi praesentium aut ad dolorum quasi. Excepturi cupiditate aut laboriosam voluptatibus qui dolores nam. Dolore deleniti voluptatem similique nihil quia fugiat quis omnis. Et quod esse consequatur. Eum voluptates quisquam quia aliquam omnis.',
           'price' => '320.43',
           'count' => '88',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '16',
           'slug' => 'mollitia-quo-quis-occaecati-in-ad',
           'category_id' => '5',
           'discount_id' => '81',
           'title' => 'Watsica, Schuster and Lueilwitz',
           'desc' => 'Sunt magnam possimus libero repellat. Aspernatur unde qui omnis atque atque voluptates. Vero quam ipsum ea. Eum similique sunt iusto et. Unde rem laborum quia eveniet. Harum eum nesciunt soluta soluta. Cum consectetur dicta sed laboriosam. Nobis exercitationem nihil delectus ad est et quis. Enim amet quidem accusantium sit ut et earum quia. Quibusdam quis eaque sit provident quia suscipit. Eos molestiae voluptas enim maiores. Cupiditate voluptatem nihil doloribus ab accusamus placeat. Ea qui enim culpa iusto iste. Quaerat aut illo quidem. Quo eius sit rerum est deserunt. Beatae aliquam in a. Est aspernatur culpa quaerat quis. Eveniet facere recusandae voluptatibus repudiandae. Omnis aperiam saepe dignissimos dolores magnam. Voluptatem occaecati voluptatum necessitatibus voluptates molestias. Beatae voluptatem iusto corrupti autem sequi eligendi est. Nihil non rerum omnis rerum molestiae. Voluptatem est ea ut tempore et. Delectus ullam alias qui qui.',
           'price' => '373.37',
           'count' => '98',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '17',
           'slug' => 'quibusdam-odit-dolor-facere',
           'category_id' => '5',
           'discount_id' => '9',
           'title' => 'Hegmann-Paucek',
           'desc' => 'Sunt sit nulla est non deserunt non incidunt. Velit facere amet quos beatae. Sed unde eius vel et nostrum autem. Error illum cum perspiciatis labore. Et aut omnis aut omnis. Est doloribus sed vitae maiores minima architecto impedit dolorum. Dolor accusamus nesciunt quibusdam id. Iste adipisci aut in nam autem. Quas voluptate eaque consectetur id esse ex. Impedit voluptatem ut molestiae totam. Qui debitis voluptas consectetur quia. Nulla rem rerum voluptatibus. Iure excepturi ut et. Modi nesciunt eum quia dolores repellendus. Eaque reprehenderit id in amet et iste nesciunt. Eum quia iusto perspiciatis reiciendis alias ea eum. Debitis fugit excepturi architecto. Aliquid consequuntur saepe dicta autem sunt. Omnis qui libero minus dicta. Sed nesciunt aperiam nam sit. Quo aut expedita quis. Quo modi non quia nemo quia officiis dolore totam. Impedit sunt facilis aspernatur quia debitis. Eum velit architecto pariatur quia pariatur. Vero eos non fugiat natus ducimus ex rerum.',
           'price' => '306.22',
           'count' => '57',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '18',
           'slug' => 'aperiam-itaque-et-commodi-sed',
           'category_id' => '4',
           'discount_id' => '100',
           'title' => 'Reilly PLC',
           'desc' => 'In consequuntur commodi necessitatibus. Necessitatibus consequuntur adipisci voluptas tenetur eum. Labore harum inventore occaecati dolorum. Molestiae in accusamus ducimus suscipit aut. Porro voluptate consectetur sed. Repellendus exercitationem ex et. Velit libero neque mollitia cum. Dolorem accusamus vitae et perferendis enim. Est incidunt quas ut possimus quo vel architecto. Error doloribus dolorem aliquam facilis. Nam itaque est nemo sed dolor dolor. Earum non aperiam reprehenderit quaerat. Aperiam ad dolor mollitia at. Voluptates facilis nihil asperiores laborum voluptas et. Voluptatum eligendi quas sint eveniet. Et repudiandae voluptas animi incidunt consequatur. Molestias rerum id quod et aut nulla placeat. Nobis molestias dolorem non voluptate consequatur. Rem tempore consequatur ratione accusantium unde pariatur consequuntur ut. Quia perferendis est quaerat rerum hic voluptatem.',
           'price' => '325.62',
           'count' => '15',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '19',
           'slug' => 'consequatur-aperiam-optio-quo-totam-dolorem-occaecati',
           'category_id' => '5',
           'discount_id' => '78',
           'title' => 'Effertz Inc',
           'desc' => 'Cum delectus et tempore omnis. Eos et exercitationem optio aut minima. Sapiente nostrum delectus dolore et consequatur maiores sunt adipisci. Eaque rerum quod laudantium molestiae ratione quod. Voluptatibus eum sed temporibus doloribus. Fugit repellendus corrupti optio quia omnis. Enim delectus minima recusandae fugiat aperiam eos assumenda. Deleniti voluptate eum et non molestias iusto libero. Non consectetur maxime accusantium veniam est non. Accusantium enim quia vel aut consectetur dolorum a. Sed officia qui necessitatibus sunt. Quasi exercitationem tenetur est et blanditiis reprehenderit. Officia incidunt qui nesciunt ipsam qui inventore consequatur. Dolores rerum quae voluptatem ut temporibus. Quo quidem suscipit autem quasi sed incidunt et. Expedita explicabo alias vero officiis. Quis accusantium et aperiam expedita repellendus amet. Accusantium rerum consequuntur aspernatur. Repellendus sit aspernatur possimus voluptas.',
           'price' => '377.43',
           'count' => '21',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '20',
           'slug' => 'sunt-delectus-ut-provident-autem-placeat-aperiam-provident',
           'category_id' => '1',
           'discount_id' => '74',
           'title' => 'Metz, Nolan and Halvorson',
           'desc' => 'Sit magni eligendi ratione possimus magni. Culpa inventore qui nesciunt eius. Voluptatem recusandae assumenda deleniti placeat excepturi. Expedita deserunt fuga id eligendi perferendis animi molestiae. In quas est nesciunt cumque aut qui. Dolorum sapiente quia dolore omnis. Nulla illo quis dolorem magnam veniam omnis. Hic eum asperiores voluptatem excepturi. Voluptas vel rerum et nobis. Culpa minima voluptates recusandae qui unde est illum. Soluta officiis expedita occaecati voluptates. Aut harum a ea voluptatem animi magni quo. Autem commodi rerum voluptas. Reprehenderit et et et cum ea ex dolorum. Ratione aperiam neque et est quibusdam. Eveniet id veniam vitae facere consequatur corrupti quod. Voluptas corrupti repudiandae quia consequuntur aperiam porro. Et quos cupiditate eos quia. Nisi sit distinctio enim sed non. Eveniet veniam eaque ut voluptatem aperiam sapiente eum. Expedita voluptatem aspernatur rerum eos saepe impedit.',
           'price' => '252.62',
           'count' => '42',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '21',
           'slug' => 'mollitia-reiciendis-dolorem-aut-aperiam-nihil-saepe-porro-omnis',
           'category_id' => '1',
           'discount_id' => '30',
           'title' => 'Mante-Gusikowski',
           'desc' => 'Eum odio error fugiat esse sint ut dignissimos. Voluptas consequatur aliquam et suscipit similique dicta et nihil. Dolores non ex veritatis qui veniam. Voluptatem vitae fuga sit atque ut. Qui placeat voluptas voluptate occaecati quisquam. Aspernatur assumenda saepe porro officiis reiciendis aut. Molestias sequi odit labore est cupiditate non aperiam adipisci. Deserunt facilis est ut distinctio quos eum. Molestiae sed placeat quia deleniti. Qui est et explicabo magni suscipit. Aliquid aliquid quaerat tempora eius. Minus quae libero facilis sint odio voluptates. Dolorum necessitatibus reprehenderit quia aliquid. Aut sunt excepturi in. Molestiae tempora enim ut laborum. Consectetur inventore quia officia consequatur. Molestias ut et autem modi rerum repellendus. Ad molestias qui alias placeat temporibus ab voluptatem. Et vel aut sapiente impedit hic ut. Sit est sit voluptas soluta odit aut. Minus aut dolorum adipisci quia enim natus et.',
           'price' => '192.44',
           'count' => '10',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '22',
           'slug' => 'quia-repellat-minus-perferendis-in-deserunt-explicabo',
           'category_id' => '1',
           'discount_id' => '38',
           'title' => 'Gusikowski-Zemlak',
           'desc' => 'Quia cupiditate et ab est. Impedit amet architecto tenetur tempora dignissimos eum. Fugiat sed neque quae voluptates. Voluptatibus consequatur ipsa qui ut. Aliquam at enim qui. Qui quisquam sint est earum. Sint facere debitis repellendus aut aut deleniti vitae. Odit distinctio facere non nisi ut. Cum et ratione velit velit aliquid maiores aut. Deserunt dolorem voluptatibus ut exercitationem ipsum. Aperiam non nam harum praesentium delectus accusamus. Itaque pariatur eaque iste at. Officia pariatur quo deserunt adipisci minus alias repellat. Dolore dolores maxime sit omnis ea est non voluptatem. Quis quia adipisci illo incidunt. Tempora qui nostrum quaerat quod. Animi fugit dolor accusantium ullam corporis consequuntur. Rerum laboriosam sequi consequatur non nostrum aliquid corporis. Veniam tempora veniam maxime eum rerum. Aut natus aspernatur facilis ipsum dolore odit recusandae nisi.',
           'price' => '304.78',
           'count' => '3',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '23',
           'slug' => 'harum-libero-est-dolor-ea-aliquam-vel',
           'category_id' => '2',
           'discount_id' => '21',
           'title' => 'Windler LLC',
           'desc' => 'Pariatur et vel repellendus vero. Ut corrupti ipsum nobis autem impedit architecto possimus. Voluptates et velit neque nisi. Eos ut voluptatem ea ad dolores molestiae. Blanditiis ipsam reiciendis rerum in sapiente error et. Atque labore natus nam delectus officiis porro aut suscipit. Quaerat unde architecto architecto laborum porro iusto. Sed quidem quis omnis. Atque totam quis in fuga est numquam. Maiores qui fugiat accusamus laboriosam. Ratione voluptate est ipsam porro voluptatem. Sit et rem optio impedit dolorum minima. Quibusdam voluptas libero exercitationem. Natus atque ea ut facere minus ipsam corrupti ea. Eaque voluptates quia aliquam molestias. Placeat quisquam repellendus at eos ducimus suscipit. Perferendis et voluptas et adipisci provident labore voluptas maxime. Exercitationem omnis natus molestias aut beatae hic excepturi ut. Dolore enim ducimus qui dolor ipsam voluptatem.',
           'price' => '65.33',
           'count' => '60',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '24',
           'slug' => 'voluptatem-molestiae-modi-corrupti-voluptatem',
           'category_id' => '4',
           'discount_id' => '28',
           'title' => 'Ernser Ltd',
           'desc' => 'Neque vitae maiores aliquid quo. Corrupti natus voluptatem accusamus culpa dolores optio in. Debitis cum doloremque vel natus sunt. Ea consequatur blanditiis sed dolore. Tempora eos dolores id est. Assumenda rem ut neque consequuntur. Voluptas at consequatur nam in deleniti nobis. Nemo ex excepturi quaerat vel consequatur. Vel amet et omnis facere. Neque veritatis quia perferendis voluptas est. Et doloremque aut cumque. Quibusdam unde voluptatem aspernatur accusantium numquam ipsa. Autem magni quos recusandae optio optio. Omnis ratione fuga quibusdam quas. Quo sapiente et quidem provident sint consectetur et. Qui velit commodi aspernatur aut sit quo. Voluptatem quidem cum eveniet voluptas quis accusamus nostrum. Et iste voluptas rem repellat blanditiis aut molestiae. Et qui cum rerum quia iure debitis molestiae.',
           'price' => '440.39',
           'count' => '61',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('products')->insert([
           'id' => '25',
           'slug' => 'sed-vitae-voluptatibus-mollitia-tenetur-aut',
           'category_id' => '4',
           'discount_id' => '10',
           'title' => 'Raynor LLC',
           'desc' => 'Rem veritatis necessitatibus nostrum hic. Perspiciatis exercitationem voluptas voluptatem dolores repellat. Asperiores aspernatur voluptatem animi. Dolore consequuntur molestiae dolorum veniam. Fugiat laudantium dolorem et earum est magnam. Id quos rem quasi. Quia necessitatibus nam iusto adipisci qui. Dolorum sed aut rem sunt sit quos. Excepturi et voluptas nam enim exercitationem et. At aliquid ab ullam labore veniam odit cumque. Voluptatem sunt cupiditate asperiores praesentium. Et molestiae et cum. Qui nisi similique natus consequuntur et. Enim amet enim sit quis sit et fuga dolores. Adipisci eos qui sint nobis eum. Qui magnam dolor optio similique modi fuga velit. Tempora quasi quo omnis impedit. Autem magnam ullam dolor saepe. Autem consequatur neque repellat fugit pariatur itaque odio. Possimus est dignissimos rerum et. Non quisquam dolor eos placeat. Consequatur vitae deserunt consequatur veritatis hic mollitia ea. Debitis cum eum molestiae.',
           'price' => '234.67',
           'count' => '1',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);

    }
}
        