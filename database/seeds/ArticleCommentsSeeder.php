
<?php

use Illuminate\Database\Seeder;

class ArticleCommentsSeeder extends Seeder
{
    public function run()
    {
       DB::table('article_comments')->insert([
           'id' => '1',
           'article_id' => '14',
           'user_id' => '1',
           'comment' => 'sgsdgasdgasgasdg',
           'views' => '244',
           'likes' => '34',
           'updated_at' => '2018-11-26 05:28:47',
       ]);
       DB::table('article_comments')->insert([
           'id' => '3',
           'article_id' => '14',
           'user_id' => '1',
           'comment' => 'hdfghdfh',
           'views' => '4',
           'likes' => '0',
           'created_at' => '2018-11-26 04:44:00',
           'updated_at' => '2018-11-26 05:28:47',
       ]);
       DB::table('article_comments')->insert([
           'id' => '7',
           'article_id' => '14',
           'user_id' => '1',
           'comment' => 'sit. Provident unde corrupti magni perferendis qui. Accusamus adipisci sit maxime ab dolorem odit. Eos sint eos unde in atque et labore. Aut architecto ut ut velit fugiat omnis. Non eius animi vero ducimus libero. Omnis debitis ducimus possimus et aperiam. Aut soluta ex et alias veritatis omnis ut. Accusantium nesciunt ipsa qui labore repellendus illum voluptas. Ratione sunt corporis fugiat nostrum dignissimos. Hic possimus voluptatem sint fugiat corporis. Qui reiciendis laborum necessitatibus voluptas. Et ab earum enim nihil dignissimos qui. Quidem debitis architecto sunt est. Quos non cumque rerum perspiciatis consequatur consequatur. Enim nostrum ea eos. Illo ab doloremque odit deleniti et. Ex ea explicabo tempora ea dolor officiis perferendis. Mollitia sapiente voluptate quae laboriosam similique. Qui consectetur tenetur quos est placeat molestias. Recusandae ducimus sit minima iusto maiores veniam rerum non. Dicta voluptatibus commodi sunt nisi ipsam. Perspiciatis neque beatae aut illum aut qui occaecati. Dolor et ut ipsam quis. Ab aut amet doloremque culpa. Labore aspernatur neque enim quia nemo. Porro nisi qui sed perferendis sunt hic nisi dolorum. Quis quis non molestiae id eius. Ut in quisquam voluptas ut eum ut. Mollitia dolor voluptatum consequatur rerum accusamus nobis animi. Ut ut eaque fuga beatae sequi qui non ut. At qui numquam tempore est dolorem eligendi magnam.',
           'views' => '4',
           'likes' => '1',
           'created_at' => '2018-11-26 05:22:47',
           'updated_at' => '2018-11-26 05:28:47',
       ]);
       DB::table('article_comments')->insert([
           'id' => '10',
           'article_id' => '14',
           'user_id' => '2',
           'comment' => 'sdfgsdg',
           'views' => '4',
           'likes' => '0',
           'created_at' => '2018-11-26 05:26:20',
           'updated_at' => '2018-11-26 05:28:47',
       ]);
       DB::table('article_comments')->insert([
           'id' => '11',
           'article_id' => '14',
           'user_id' => '2',
           'comment' => 'sdfgsdfg',
           'views' => '4',
           'likes' => '0',
           'created_at' => '2018-11-26 05:26:23',
           'updated_at' => '2018-11-26 05:28:47',
       ]);
       DB::table('article_comments')->insert([
           'id' => '12',
           'article_id' => '14',
           'user_id' => '2',
           'comment' => 'adfjajsfjasfjsdfjsfj',
           'views' => '4',
           'likes' => '0',
           'created_at' => '2018-11-26 05:26:27',
           'updated_at' => '2018-11-26 05:28:47',
       ]);

    }
}
        