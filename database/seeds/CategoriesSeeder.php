
<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    public function run()
    {
       DB::table('categories')->insert([
           'id' => '1',
           'title' => 'Bartoletti-Stokes',
           'slug' => 'tempore-et-et-voluptas-modi-consequatur-tempore-occaecati',
           'place' => '1',
           'status' => '0',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('categories')->insert([
           'id' => '2',
           'title' => 'Gaylord-Lynch',
           'slug' => 'quibusdam-quia-minima-reiciendis-ea-consequatur-sed-optio',
           'place' => '1',
           'status' => '0',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('categories')->insert([
           'id' => '3',
           'title' => 'Steuber, Koch and Ziemann',
           'slug' => 'ut-sequi-voluptas-debitis-eligendi-omnis',
           'place' => '1',
           'status' => '0',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('categories')->insert([
           'id' => '4',
           'title' => 'Boyer, Hansen and Gibson',
           'slug' => 'labore-necessitatibus-blanditiis-aliquam-excepturi-asperiores-dolorem',
           'place' => '1',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);
       DB::table('categories')->insert([
           'id' => '5',
           'title' => 'Flatley LLC',
           'slug' => 'harum-sunt-hic-non-ab-asperiores',
           'place' => '1',
           'status' => '1',
           'created_at' => '2018-11-26 03:35:54',
           'updated_at' => '2018-11-26 03:35:54',
       ]);

    }
}
        