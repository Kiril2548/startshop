
<?php

use Illuminate\Database\Seeder;

class MigrationsSeeder extends Seeder
{
    public function run()
    {
       DB::table('migrations')->insert([
           'id' => '1',
           'migration' => '2014_10_12_000000_create_users_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '2',
           'migration' => '2014_10_12_100000_create_password_resets_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '3',
           'migration' => '2018_06_21_210618_create_roles_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '4',
           'migration' => '2018_11_02_153031_create_categories_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '5',
           'migration' => '2018_11_05_181643_create_products_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '6',
           'migration' => '2018_11_05_182727_create_product_imgs_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '7',
           'migration' => '2018_11_05_182848_create_product_params_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '8',
           'migration' => '2018_11_06_125025_create_sessions_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '9',
           'migration' => '2018_11_06_130400_create_settings_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '10',
           'migration' => '2018_11_08_140423_create_discounts_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '11',
           'migration' => '2018_11_08_140437_create_carts_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '12',
           'migration' => '2018_11_08_140451_create_orders_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '13',
           'migration' => '2018_11_24_012104_create_articles_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '14',
           'migration' => '2018_11_24_012126_create_news_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '15',
           'migration' => '2018_11_24_012136_create_pages_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '16',
           'migration' => '2018_11_24_012448_create_article_comments_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '17',
           'migration' => '2018_11_24_012520_create_product_comments_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '18',
           'migration' => '2018_11_24_050318_create_api_settings_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '19',
           'migration' => '2018_11_24_050323_create_payments_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '20',
           'migration' => '2018_11_24_050358_create_permissions_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '21',
           'migration' => '2018_11_24_050434_create_admin_settings_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '22',
           'migration' => '2018_11_24_053149_create_texts_table',
           'batch' => '1',
       ]);
       DB::table('migrations')->insert([
           'id' => '23',
           'migration' => '2018_11_24_064018_create_article_categories_table',
           'batch' => '1',
       ]);

    }
}
        