<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSettingsSeeder::class);

        $this->call(LaratrustSeeder::class);
        $this->call(SettingsSeeder::class);


        //$this->call(ArticleCommentsSeeder::class);
        //$this->call(ProductsSeeder::class);
        //$this->call(ProductParamsSeeder::class);
        //$this->call(ProductImgsSeeder::class);

        $this->call(RolesSeeder::class);
        $this->call(UsersRolesSeeder::class);
    }
}
