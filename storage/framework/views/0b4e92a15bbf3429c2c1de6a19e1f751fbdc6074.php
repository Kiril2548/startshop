<b-field label="Заголовок"
         :type="errors.title ? 'is-danger' : ''"
         :message="errors.title ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="article.title" maxlength="250" placeholder="Дикий тигр"></b-input>
</b-field>

<b-field label="Текст"
         :type="errors.text ? 'is-danger' : ''"
         :message="errors.text ? 'Поле обязательно к заполнению' : ''">
    <b-input type="textarea" v-model="article.text" maxlength="50000" placeholder="Какой-то текст"></b-input>
</b-field>



<div class="container-fluid" style="padding: 0;">
    <div class="row">
        <div class="col-md-6">
            <b-field
                label="Фото"
                :type="errors.img ? 'is-danger' : ''"
                :message="errors.img ? 'Фото должно быть добавлненно' : ''"
            >
                <button v-if="article.img == ''" class="button" @click="lfmm">Добавить фото</button>
            </b-field>
            <div class="container-fluid" v-if="article.img != ''" style="margin-top: 5px;">
                <div class="row">
                    <span style="">
                        <img :src="article.img"
                             style="height: 100px; width: 100px; object-fit: cover; border-radius: 10px;"
                             alt="">
                        <br>
                        <button  @click="delImgFromArray(key)" style="margin-top: 5px; margin-left: 10px;"
                                 class="button is-danger is-inverted">Удалить</button>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <b-field label="Категория"
                     :type="errors.article_category_id ? 'is-danger' : ''"
                     :message="errors.article_category_id ? 'Поле обязательно к заполнению' : ''">
                <b-select v-model="article.article_category_id" placeholder="Веберите категорию">
                    <option v-for="item in categories" :value="item.id" v-text="item.title"></option>
                </b-select>
            </b-field>
        </div>
    </div>
</div>

<div style="text-align: right;">
    <a class="button is-success" @click="onSave">
                                <span class="icon is-small">
                                  <i class="fas fa-check"></i>
                                </span>
        <span>Save</span>
    </a>
</div>
