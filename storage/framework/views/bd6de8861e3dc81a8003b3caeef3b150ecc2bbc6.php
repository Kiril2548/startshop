<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="notification row">
            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="<?php echo e(route('admin.main')); ?>">Админка</a></li>
                        <li><a href="<?php echo e(route('admin.admin_settings.index')); ?>">Настройки админки</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card" style="padding: 15px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <h2 style="font-size: 15px; font-weight: 800;">Default</h2>
                                <br>
                                <div class="field">
                                    <b-checkbox v-model="setting['e_commerce_d']">
                                        E-commerce админка
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['blog_d']">
                                        Blog админка
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['session']">
                                        Сесии
                                    </b-checkbox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h2 style="font-size: 15px; font-weight: 800;">E-commerce</h2>
                                <br>
                                <div class="field">
                                    <b-checkbox v-model="setting['category']">
                                        Категории (продукты)
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['products']">
                                        Продукты/Сервисы
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['orders']">
                                        Заказы
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['payments']">
                                        Транзакции
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['carts']">
                                        Карзина
                                    </b-checkbox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <h2 style="font-size: 15px; font-weight: 800;">Content</h2>
                                <br>
                                <div class="field">
                                    <b-checkbox v-model="setting['pages']">
                                        Страницы
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['news']">
                                        Новости
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['article_categories']">
                                        Категории (статьи)
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['articles']">
                                        Статьи
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['texts']">
                                        Текста
                                    </b-checkbox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h2 style="font-size: 15px; font-weight: 800;">Settings</h2>
                                <br>
                                <div class="field">
                                    <b-checkbox v-model="setting['settings']">
                                        Настройки
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['api_settings']">
                                        Api-Настройки
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['roles']">
                                        Роли
                                    </b-checkbox>
                                </div>
                                <div class="field">
                                    <b-checkbox v-model="setting['permissions']">
                                        Права
                                    </b-checkbox>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div style="text-align: right;">
                        <a class="button is-success" @click="onSave">
                                <span class="icon is-small">
                                  <i class="fas fa-check"></i>
                                </span>
                            <span>Save</span>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startPush('scripts'); ?>
    <script>
        new Vue({
            el: "#content",
            data: {
                setting: <?php echo json_encode($admin_settings, 15, 512) ?>,
            },
            methods: {
                onSave() {
                    let self = this;
                    axios.put(route('admin.admin_settings.update'), self.setting)
                        .then(function (response) {
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Настройки админки успешно обновлена`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>