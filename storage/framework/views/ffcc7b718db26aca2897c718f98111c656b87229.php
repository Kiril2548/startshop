<?php $__env->startSection('content'); ?>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    <?php echo e(config('app.name', 'MineStartProject')); ?>

                </div>

                <div class="links">
                    
                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>