<b-field label="Название"
         :type="errors.title ? 'is-danger' : ''"
         :message="errors.title ? 'Поле обязательно к заполнению' : ''">
    <b-input v-model="category.title" maxlength="50" placeholder="Категория"></b-input>
</b-field>
<b-field
    label="Категория">
    <b-select v-model="category.category_id" placeholder="Выберите родительскую категорию"
              expanded>
        <option value="">NONE</option>
        <option v-for="category in categories" :value="category.id">{{ category.title }}</option>
    </b-select>
</b-field>
<b-field
    label="Статус">
    <b-select v-model="category.status" placeholder="ACTIVE" expanded>
        <option value="1">ACTIVE</option>
        <option value="0">DELETE</option>
    </b-select>
</b-field>

<div style="text-align: right;">
    <a class="button is-success" @click="onSave">
                                <span class="icon is-small">
                                  <i class="fas fa-check"></i>
                                </span>
        <span>Save</span>
    </a>
</div>
