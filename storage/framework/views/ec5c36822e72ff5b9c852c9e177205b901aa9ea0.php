<?php $__env->startSection('content'); ?>
    <div class="container mt-3 mb-3">
        <div class="row">
            <div class="col-md-12 pb-4" style="border-bottom: 1px solid #7BC043;">
                <h4>Фильтры:</h4>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 p-0">
                            <div class="form-group">
                                <label for="search1">Поиск</label>
                                <div class="input-group">
                                    <input id="search1" type="text" class="form-control" placeholder=""
                                           aria-describedby="button-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i
                                                class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Позицый</label>
                                            <select class="form-control" id="exampleFormControlSelect1">
                                                <option>10</option>
                                                <option>20</option>
                                                <option>50</option>
                                                <option>100</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Сорт.</label>
                                            <select class="form-control" id="exampleFormControlSelect1">
                                                <option>По цене с низ.</option>
                                                <option>По цене с выс.</option>
                                                <option>По названию A-z</option>
                                                <option>По названию с Z-a</option>
                                                <option>По дате добавления</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 p-2">
                <button class="btn w-100 btn-secondary btn-sm mb-1" type="button" data-toggle="collapse"
                        data-target="#collapseExample"
                        aria-expanded="false" aria-controls="collapseExample">
                    Категории
                </button>
                <div class="collapse" id="collapseExample">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="<?php echo e(route('shop__', ['category' => 'all_category'])); ?>" style="
                                display: block;
                                text-align: left;
                                height: 100%;
                                color: #666;
                                text-decoration: none;
                                padding: 10px;
                            ">Все категории</a>
                        </li>
                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('shop__', ['category' => $item->slug])); ?>" style="
                                display: block;
                                text-align: left;
                                height: 100%;
                                color: #666;
                                text-decoration: none;
                                padding: 10px;
                            "><?php echo e($item->title); ?></a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="container-fluid">
                    <div class="row">
                        <?php $__currentLoopData = $main_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-lg-4 col-md-6 col-sm-12 p-2">
                                <div class="card card-shop w-100 p-3 text-center">
                                    <img class="img-preview" style="max-height: 200px;"
                                         src="<?php echo e($product->imgs[0]->url); ?>" alt="">
                                    <h5><?php echo e($product->title); ?></h5>
                                    <sup class="mt-1"><?php echo e($product->category->title); ?></sup>
                                    <br>
                                    <?php if(isset($product->discount)): ?>
                                        <span style="
                                            position: absolute;
                                            height: 40px;
                                            width: 40px;
                                            padding: 5px;
                                            padding-top: 14px;
                                            border-radius: 50%;
                                            right: 10px;
                                            top: 10px;
                                        " class="badge badge-danger">-<?php echo e($product->discount->value); ?>%</span>
                                        <h6>
                                            <sup style="text-decoration: line-through;">
                                                <?php echo e($product->price); ?><?php echo e($settings['currency']); ?>

                                            </sup>
                                            <?php echo e($product->price - ($product->price*($product->discount->value/100))); ?><?php echo e($settings['currency']); ?>

                                        </h6>
                                    <?php else: ?>
                                        <h6><?php echo e($product->price); ?><?php echo e($settings['currency']); ?></h6>
                                    <?php endif; ?>
                                    <hr>
                                    <div class="btn-group d-flex justify-content-center">
                                        <a href="" style="color: #000 !important;" class="btn btn-sm btn-primary"><i
                                                class="fas fa-eye"></i> Подробнее</a>
                                        <a href="" style="color: #000 !important;" class="btn btn-sm btn-success">Заказть
                                            <i class="fas fa-shopping-cart"></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="w-100 d-flex justify-content-center">
                            <?php echo e($main_products->links()); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>