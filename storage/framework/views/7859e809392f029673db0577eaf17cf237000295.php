<nav id="nav" class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
            <img src="<?php echo e(asset('favicon.ico')); ?>" width="35" height="35"
                 style="margin-right: 5px;"> <?php echo e(config('app.name', 'MineStartProject')); ?>

        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="<?php echo e(__('Toggle navigation')); ?>">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo e(route('main')); ?>"><?php echo e(__('HOME')); ?></a>
                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <?php echo e(__('SHOP')); ?> <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo e(route('shop')); ?>">
                            <?php echo e(__('SHOP WITH CART')); ?>

                        </a>
                        <a class="dropdown-item" href="<?php echo e(route('shop_')); ?>">
                            <?php echo e(__('SHOP WITH OUT CART')); ?>

                        </a>
                        <a class="dropdown-item" href="<?php echo e(route('shop__')); ?>">
                            <?php echo e(__('CATALOG')); ?>

                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <?php echo e(__('BLOG')); ?> <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo e(route('articles.index')); ?>">
                            <?php echo e(__('DEFAULT')); ?>

                        </a>
                        <a class="dropdown-item" href="<?php echo e(route('articles.mini')); ?>">
                            <?php echo e(__('COMPACT')); ?>

                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <?php echo e(__('NEWS')); ?> <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo e(route('news.index')); ?>">
                            <?php echo e(__('DEFAULT')); ?>

                        </a>
                        <a class="dropdown-item" href="<?php echo e(route('news.mini')); ?>">
                            <?php echo e(__('COMPACT')); ?>

                        </a>
                        <a class="dropdown-item" href="<?php echo e(route('news.mini_basic')); ?>">
                            <?php echo e(__('MINI')); ?>

                        </a>
                    </div>
                </li>

                <?php if(count($pages) >= 1): ?>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <?php echo e(__('PAGES')); ?> <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <a class="dropdown-item" href="<?php echo e(route('pages.show', $page->slug)); ?>">
                                    <?php echo e(__($page->title)); ?>

                                </a>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </li>
                <?php endif; ?>

                <li class="nav-item">
                    <a class="nav-link" href="#">|</a>
                </li>
                <?php if(auth()->guard()->guest()): ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(route('login')); ?>"><?php echo e(__('LOGIN')); ?></a>
                    </li>
                    <li class="nav-item">
                        <?php if(Route::has('register')): ?>
                            <a class="nav-link" href="<?php echo e(route('register')); ?>"><?php echo e(__('REGISTER')); ?></a>
                        <?php endif; ?>
                    </li>
                <?php else: ?>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                            <?php if(Auth::user()->hasRole('admin')): ?>
                                <a class="dropdown-item" href="<?php echo e(route('admin.main')); ?>">
                                    <?php echo e(__('Admin')); ?>

                                </a>
                                <hr>
                            <?php endif; ?>

                            <a class="dropdown-item" href="<?php echo e(route('user.show', Auth::user()->id)); ?>">
                                <?php echo e(__('Profile')); ?>

                            </a>

                            <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <?php echo e(__('Logout')); ?>

                            </a>

                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST"
                                  style="display: none;">
                                <?php echo csrf_field(); ?>
                            </form>
                        </div>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>
