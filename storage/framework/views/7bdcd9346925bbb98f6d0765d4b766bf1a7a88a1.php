<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="notification row">

            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="<?php echo e(route('admin.main')); ?>">Админка</a></li>
                        <li><a href="<?php echo e(route('admin.products.index')); ?>">Продукты</a></li>
                        <li><a href="#">Редактирование продукта: <?php echo e($product->title); ?></a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card" style="padding: 15px;"><?php echo $__env->make('admin.product._form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?></div>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startPush('scripts'); ?>
    <script>
        var lfm = function (options, cb) {

            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=1280,height=600');
            window.SetUrl = cb;
        }
    </script>
    <script>
        var slug = <?php echo json_encode($product->slug, 15, 512) ?>;
        var vue = new Vue({
            el: "#content",
            data: {
                errors: {},
                imgs: <?php echo json_encode($imgs, 15, 512) ?>,
                params: <?php echo json_encode($params, 15, 512) ?>,
                param: {
                    key: 'param',
                    value: '',
                },
                product: <?php echo json_encode($product, 15, 512) ?>,
                categories: <?php echo json_encode($categories, 15, 512) ?>,
                discounts: <?php echo json_encode($discounts, 15, 512) ?>
            },
            methods: {
                lfmm() {
                    let self = this;
                    var img_url = lfm({type: 'image', prefix: ''}, function (url, path) {
                        self.addImgToArray(url);
                    });
                },
                addImgToArray(url) {
                    this.imgs.push({url: url});
                },
                delImgFromArray(key) {
                    this.imgs.splice(key, 1)
                },
                addParam() {
                    if (this.param.key != '' && this.param.value != '') {
                        this.params.push(this.param);
                        this.param = {
                            key: 'param',
                            value: '',
                        };
                        this.$toast.open({
                            message: `Параметер добавлен`,
                            duration: 1000,
                            queue: false,
                            position: 'is-bottom',
                            type: 'is-success',
                        });
                    }
                },
                delParam(key) {
                    this.params.splice(key, 1);
                },
                onSave() {
                    let self = this;
                    axios.put(route('admin.products.update', self.product.slug), {
                        product: self.product,
                        params: self.params,
                        imgs: self.imgs,
                    })
                        .then(function (response) {
                            self.product = response.data;
                            self.params = response.data.params;
                            self.param = {
                                key: 'param',
                                value: '',
                            };
                            self.imgs = response.data.imgs;
                            self.errors = {};
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Продукт успешно сохранена`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                            setTimeout(function () {
                                if (response.data.slug != slug) {
                                    window.location = route('admin.products.edit', response.data.slug);
                                }
                            }, 1500);
                        })
                        .catch(function (error) {
                            self.errors = {};
                            let response = error.response;
                            if(response.status === 422) {

                                self.errors = response.data.errors;
                                Object.keys(response.data.errors).map((key) => {
                                    self.errors[key] = response.data.errors[key][0];
                                });
                            }
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
