<?php $__env->startPush('styles'); ?>
    <style>
        .img-preview-table:hover {
            cursor: pointer;
        }
    </style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="notification row">

            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="<?php echo e(route('admin.main')); ?>">Админка</a></li>
                        <li><a href="<?php echo e(route('admin.products.index')); ?>">Продукты</a></li>
                    </ul>
                </nav>
            </div>

            <div class="card col-md-12" style="padding: 5px;">
                <header class="card-header" style="padding: 0; padding-bottom: 5px; padding-top: 5px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <b-field
                                    label="Позицый на страницу">
                                    <b-select v-model="per_page" expanded>
                                        <option v-for="item in pre_page_a" :value="item">{{ item }}</option>
                                    </b-select>
                                </b-field>
                            </div>
                            <div class="col-md-4">
                                <b-field
                                    label="Поиск">
                                    <div class="field has-addons">
                                        <div class="control" style="width: 100%;">
                                            <input class="input" type="text" v-model="filter" placeholder="Поиск">
                                        </div>
                                        <div class="control">
                                            <a class="button is-primary is-outlined" @click="onSearch(filter)">
                                                Найти
                                            </a>
                                        </div>
                                    </div>
                                </b-field>
                            </div>
                            <div class="col-md-4">
                                <button class="button is-primary is-smal"
                                        style="float: right; margin-bottom: 10px; margin-top: 15px;"
                                        @click="isCardModalActive = true">
                                    Добавить товар
                                </button>
                                <b-modal :active.sync="isCardModalActive" :width="640" scroll="keep">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="content">
                                                <?php echo $__env->make('admin.product._form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </b-modal>
                            </div>
                        </div>
                    </div>
                </header>
                <b-table
                    :data="data"
                    paginated
                    hoverable
                    mobile-cards
                    :filter="filter"
                    :per-page="per_page"
                    :opened-detailed="defaultOpenedDetails"
                    detailed
                    detail-key="id"
                    @details-open="(row, index) => $toast.open(`Описание ${row.title} открыто`)">
                    <template slot-scope="props">

                        <b-table-column field="id" label="ID" width="40" sortable numeric>
                            {{ props.row.id }}
                        </b-table-column>

                        <b-table-column field="img" label="Картинка" width="60" sortable numeric>
                            <img @click="imageModal(props.row.imgs[0].url)" class="img-preview-table"
                                 :src="props.row.imgs[0].url" alt="" style="height: 30px; width: 45px; object-fit: cover;">
                        </b-table-column>

                        <b-table-column field="title" label="Название" sortable>
                            {{ props.row.title }}
                        </b-table-column>

                        <b-table-column field="category_id" label="Категория" sortable>
                            <span>{{ props.row.category.title }}</span>
                        </b-table-column>

                        <b-table-column field="price" label="Цена" sortable>
                            <b>{{ props.row.price }}</b>{{settings['currency']}}
                        </b-table-column>

                        <b-table-column field="status" label="Статус" sortable centered>
                            <span class="tag is-success" v-if="props.row.status == 1">
                                ACTIVE
                            </span>
                            <span class="tag is-danger" v-if="props.row.status == 0">
                                DELETE
                            </span>
                        </b-table-column>

                        <b-table-column field="action" label="" sortable numeric>
                            <div class="btn-group">
                                <a :href="routeEdit+ '/' + props.row.slug + '/edit'"
                                   class="btn btn-outline-primary btn-sm">EDIT</a>
                                <button @click="onDelete(props.row.slug)" class="btn btn-outline-danger btn-sm"
                                        v-if="props.row.status == 1">DELETE
                                </button>
                                <button @click="onDelete(props.row.slug)" class="btn btn-outline-success btn-sm"
                                        v-if="props.row.status == 0">ACTIVATE
                                </button>
                            </div>
                        </b-table-column>
                    </template>
                    <template slot="empty">
                        <section class="section">
                            <div class="content has-text-grey has-text-centered">
                                <p>
                                    <b-icon
                                        icon="emoticon-sad"
                                        size="is-large">
                                    </b-icon>
                                </p>
                                <p>Ничего нет.</p>
                            </div>
                        </section>
                    </template>
                    <template slot="detail" slot-scope="props">
                        <article class="media">
                            <div class="media-content">
                                <div class="content">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>
                                                    <img v-if="props.row.imgs != null"
                                                         style="width: 240px; object-fit: cover"
                                                         :src="props.row.imgs[0].url">
                                                    <br>
                                                    <br>
                                                    <strong>{{ props.row.title }}</strong> <sup>{{
                                                        props.row.category.title }}</sup>
                                                    <br>
                                                    <span v-html="props.row.desc"></span>
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <h5>Параметры продукта</h5>
                                                <ul style="list-style: none;">
                                                    <li v-for="param in props.row.params">
                                                        <b>{{ param.value }}</b>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-2">
                                                <h5>Цены продукта</h5>
                                                <ul style="list-style: none;">
                                                    <li>Цена : <b>{{ props.row.price }}</b>{{settings['currency']}}
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-2">
                                                <h5>Дополнительная информация</h5>
                                                <ul style="list-style: none;">
                                                    <li>Статус :
                                                        <span class="tag is-success" v-if="props.row.status == 1">
                                                            ACTIVE
                                                        </span>
                                                        <span class="tag is-danger" v-if="props.row.status == 0">
                                                            DELETE
                                                        </span>
                                                    </li>
                                                    <li>Фото : <b class="tag is-info">{{ props.row.imgs.length }}</b></li>

                                                    <li> Скидка :
                                                        <span class="tag is-warning" v-if="props.row.discount_id != null">
                                                            <b>{{ props.row.discount.value }}%</b>
                                                        </span>
                                                        <span class="tag" v-if="props.row.discount_id == null">
                                                            <b>0%</b>
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </template>
                </b-table>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startPush('scripts'); ?>
    <script>
        var lfm = function (options, cb) {

            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=1440,height=960');
            window.SetUrl = cb;
        }
    </script>
    <script>
        var vue = new Vue({
            el: "#content",
            data: {
                routeEdit: route('admin.products.index'),
                filter: '',
                per_page: 10,
                pre_page_a: [5, 10, 20, 50, 100],
                product: {
                    status: 1,
                },
                imgs: [],
                params: [],
                param: {
                    key: 'param',
                    value: '',
                },
                defaultOpenedDetails: [],
                isCardModalActive: false,
                data: <?php echo json_encode($products, 15, 512) ?>,
                products: <?php echo json_encode($products, 15, 512) ?>,
                categories: <?php echo json_encode($categories, 15, 512) ?>,
                settings: <?php echo json_encode($settings, 15, 512) ?>,
                discounts: <?php echo json_encode($discounts, 15, 512) ?>,
                errors: {}
            },
            methods: {
                imageModal(url) {
                    this.$modal.open(
                        `<p class="image is-4by3">
                            <img style="object-fit: cover;" src="` + url + `">
                        </p>`
                    )
                },
                lfmm() {
                    let self = this;
                    var img_url = lfm({type: 'image', prefix: ''}, function (url, path) {
                        self.addImgToArray(url);
                    });
                },
                addImgToArray(url) {
                    this.imgs.push({url: url});
                },
                delImgFromArray(key) {
                    this.imgs.splice(key, 1)
                },
                addParam() {
                    if (this.param.key != '' && this.param.value != '') {
                        this.params.push(this.param);
                        this.param = {
                            key: 'param',
                            value: '',
                        };
                        this.$toast.open({
                            message: `Параметер добавлен`,
                            duration: 1000,
                            queue: false,
                            position: 'is-bottom',
                            type: 'is-success',
                        });
                    }
                },
                delParam(key) {
                    this.params.splice(key, 1);
                },
                onDelete(index) {
                    let self = this;
                    axios.delete(route('admin.products.destroy', index))
                        .then(function (response) {
                            self.data = response.data;
                            self.products = response.data;
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Статус продукта успешно изменен`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                },
                onSave() {
                    let self = this;
                    axios.post(route('admin.products.store'), {
                        product: self.product,
                        params: self.params,
                        imgs: self.imgs,
                    })
                        .then(function (response) {
                            self.data = response.data;
                            self.product = {
                                status: 1,
                            };
                            self.params = [];
                            self.param = {
                                key: 'param',
                                value: '',
                            };
                            self.imgs = [];
                            self.errors = {};
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Продукт успешно сохранена`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.errors = {};
                            let response = error.response;
                            if(response.status === 422) {

                                self.errors = response.data.errors;
                                Object.keys(response.data.errors).map((key) => {
                                    self.errors[key] = response.data.errors[key][0];
                                });
                            }
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                },
                onSearch(search) {
                    let self = this;
                    axios.post(route('admin.products.search'), {
                        search: search
                    })
                        .then(function (response) {
                            self.data = response.data;
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Поиск прошел успешно`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время поиска произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>