<?php $__env->startSection('content'); ?>


    <div class="container mb-2 mt-2">
        <div class="row">
            <div class="col-md-9">
                <div class="card img-card">
                    <div class="img"
                         style="
                             min-height: 400px;
                             background-size: cover;
                             background-position: center;
                             background-repeat: no-repeat;
                             background-image: url(<?php echo e($article->img); ?>);
                             "></div>
                </div>
                <div class="card card-info p-4 text-justify" style="margin-top: -150px; width: 94%; margin-left: 3%;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 text-left"><h3><?php echo e($article->title); ?></h3></div>
                            <div class="col-md-6 text-right">
                                <span>
                                    <span class="mr-3"><i class="fas fa-eye"></i> <?php echo e($article->views); ?></span>
                                    <span class="mr-3"><i
                                            class="fas fa-comments"></i> <?php echo e(count($article->comments)); ?></span>
                                    <span><i class="fas fa-clock"></i> <?php echo e($article->created_at); ?></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <p><?php echo e($article->text); ?></p>
                </div>
                <div class="card mt-3" style="width: 94%; margin-left: 3%;">
                    <?php if(auth()->guard()->check()): ?>
                        <form action="<?php echo e(route('comments.store', $article->slug)); ?>" method="post" class="p-3">
                            <?php echo csrf_field(); ?>
                            <div class="input-group mb-3">
                            <textarea required name="comment" class="form-control" placeholder="Добавить комментария"
                                      rows="2"
                                      aria-label="With textarea"></textarea>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i
                                            class="fab fa-telegram-plane"></i></button>
                                </div>
                            </div>
                        </form>
                    <?php endif; ?>

                    <?php if(count($comments) >= 1): ?>
                        <div class="container">
                            <div class="row p-3">
                                <?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="media comment-box">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="img-responsive user-photo"
                                                     src="<?php echo e($comment->user->avatar); ?>">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="container-fluid media-heading">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <h5><?php echo e($comment->user->name); ?></h5>
                                                    </div>
                                                    <div class="col-md-4 text-right">
                                                        <span><i class="fas fa-eye"></i> <?php echo e($comment->views); ?></span>
                                                        <span class="ml-2"><i
                                                                class="fas fa-thumbs-up"></i> <?php echo e($comment->likes); ?></span>
                                                        <span class="ml-2">
                                                            <?php if(Auth::check()): ?>

                                                                <?php if($comment->user_id == Auth::user()->id): ?>
                                                                    <div class="btn-group" role="group">
                                                                    <button type="button"
                                                                            class="btn btn-outline-secondary"
                                                                            data-toggle="modal"
                                                                            data-target="#comment_<?php echo e($comment->id); ?>">
                                                                        <i class="fas fa-edit"></i>
                                                                    </button>
                                                                    <button type="button"
                                                                            class="btn btn-outline-secondary"
                                                                            onclick="document.getElementById('del_comment_<?php echo e($comment->id); ?>').submit()">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </button>
                                                                    <form id="del_comment_<?php echo e($comment->id); ?>"
                                                                          action="<?php echo e(route('comments.delete', $comment->id)); ?>"
                                                                          method="post">
                                                                        <?php echo csrf_field(); ?>
                                                                    </form>
                                                                </div>
                                                                    <!-- Modal -->
                                                                    <div class="modal fade"
                                                                         id="comment_<?php echo e($comment->id); ?>"
                                                                         tabindex="-1" role="dialog"
                                                                         aria-labelledby="exampleModalCenterTitle"
                                                                         aria-hidden="true">
                                                                    <div class="modal-dialog modal-dialog-centered"
                                                                         role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title"
                                                                                    id="exampleModalCenterTitle">Редактирование комментария</h5>
                                                                                <button type="button" class="close"
                                                                                        data-dismiss="modal"
                                                                                        aria-label="Close">
                                                                                    <span
                                                                                        aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <form
                                                                                    action="<?php echo e(route('comments.update', $comment->id)); ?>"
                                                                                    method="post" class="p-3">
                                                                                    <?php echo csrf_field(); ?>
                                                                                    <div class="input-group mb-3">
                                                                                    <textarea name="comment"
                                                                                              class="form-control"
                                                                                              placeholder="Текст комментария"
                                                                                              rows="10"
                                                                                              required
                                                                                              aria-label="With textarea">
                                                                                        <?php echo e($comment->comment); ?>

                                                                                    </textarea>
                                                                                        <div class="input-group-append">
                                                                                            <button
                                                                                                class="btn btn-outline-secondary"
                                                                                                type="submit"
                                                                                                id="button-addon2"><i
                                                                                                    class="fab fa-telegram-plane"></i></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                  </div>
                                                                </div>

                                                                <?php else: ?>
                                                                    <button type="submit"
                                                                            onclick="document.getElementById('like_comment_<?php echo e($comment->id); ?>').submit()"
                                                                            class="btn btn-sm btn-outline-secondary">
                                                                        <i class="fas fa-thumbs-up"></i>
                                                                    </button>
                                                                    <form id="like_comment_<?php echo e($comment->id); ?>"
                                                                          action="<?php echo e(route('comments.like', $comment->id)); ?>"
                                                                          method="post">
                                                                        <?php echo csrf_field(); ?>
                                                                </form>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <p>
                                                <?php echo e($comment->comment); ?>

                                                <br>
                                                <br>
                                                <sup class="fa-pull-right"><i
                                                        class="fas fa-clock"></i> <?php echo e($comment->created_at); ?></sup>
                                            </p>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-3">
                <ul class="list-group">
                    <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li class="list-group-item">
                            <a href="<?php echo e(route('articles.show', $item->slug)); ?>" style="
                                display: block;
                                text-align: right;
                                height: 100%;
                                color: #666;
                                text-decoration: none;
                                padding: 10px;
                            "><?php echo e($item->title); ?></a>
                        </li>

                        <?php if($key == 10): ?>
                            <?php break; ?>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>