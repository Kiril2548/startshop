<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Админка</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo e(asset('admin.ico')); ?>"/>

    <link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap/dist/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css"/>
    <link rel="stylesheet" href="https://unpkg.com/buefy/dist/buefy.min.css">
    <link rel="stylesheet" href="//cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo e(asset('css/admin_nav.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
    <?php echo app('Tightenco\Ziggy\BladeRouteGenerator')->generate(); ?>
</head>
<body>
<?php echo $__env->yieldPushContent('styles'); ?>
<div id="header">
    <?php echo $__env->make('admin.particles.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>

<div id="nav">
    <?php echo $__env->make('admin.particles.nav', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>

<div id="content">
    <?php echo $__env->yieldContent('content'); ?>
</div>

<div id="footer">
    <?php echo $__env->make('admin.particles.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>

<script src="https://unpkg.com/vue"></script>

<!-- Full bundle -->
<script src="https://unpkg.com/buefy/dist/buefy.min.js"></script>

<!-- Individual components -->
<script src="https://unpkg.com/buefy/dist/components/table"></script>
<script src="https://unpkg.com/buefy/dist/components/input"></script>

<!-- AXIOS -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    axios.defaults.headers.common['X-CSRF-TOKEN'] = <?php echo json_encode(csrf_token(), 15, 512) ?>;
</script>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="<?php echo e(asset('js/admin_nav.js')); ?>"></script>

<?php echo $__env->yieldPushContent('scripts'); ?>

<script>
    var header = new Vue({
        el: '#header',
        data: {
            m: "HELLO"
        }
    });

    var footer = new Vue({
        el: '#footer',
        data: {
            m: "HELLO"
        }
    });
</script>


</body>
</html>
