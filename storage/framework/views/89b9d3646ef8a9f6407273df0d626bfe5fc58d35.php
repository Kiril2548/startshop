<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <link rel="shortcut icon" type="image/ico" href="<?php echo e(asset('favicon.ico')); ?>"/>

    <title><?php echo e(config('app.name', 'MineStartProject')); ?></title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <?php echo app('Tightenco\Ziggy\BladeRouteGenerator')->generate(); ?>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script>
        jQuery(document).ready(function($) {
            $(window).load(function() {
                setTimeout(function() {
                    $('#circle').fadeOut('slow', function() {});
                }, 500);

                setTimeout(function() {
                    $('#circle-div').fadeOut('slow', function() {});
                }, 700);

            });
        });
    </script>
</head>
<body>

<div id="circle-div">
    <div id="circle">
        <div class="loader">
            <div class="loader">
                <div class="loader">
                    <div class="loader">
                        <div class="loader">
                            <div class="loader">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $__env->yieldPushContent('style'); ?>

<?php echo $__env->make('frontend.partials._style', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>



<div id="app">
    <?php echo $__env->make('frontend.partials._nav', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <main class="app">
        <?php echo $__env->yieldContent('content'); ?>
    </main>

    <?php echo $__env->make('frontend.partials._footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
<?php echo $__env->make('frontend.partials._up', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- development version, includes helpful console warnings -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>


<!-- Individual components -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>

<!-- AXIOS -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
    axios.defaults.headers.common['X-CSRF-TOKEN'] = <?php echo json_encode(csrf_token(), 15, 512) ?>;

    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 250) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {

            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });
</script>



    
        
        
            
            
            
        
        
    


<?php echo $__env->yieldPushContent('scripts'); ?>
</body>
</html>
