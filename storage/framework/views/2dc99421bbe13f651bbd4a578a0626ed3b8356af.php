<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="notification row">
            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="<?php echo e(route('admin.main')); ?>">Админка</a></li>
                        <li><a href="<?php echo e(route('admin.texts.index')); ?>">Текста</a></li>
                    </ul>
                </nav>
            </div>

            <div class="card col-md-12" style="padding: 5px;">
                <header class="card-header" style="padding: 0; padding-bottom: 5px; padding-top: 5px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <b-field
                                    label="Позицый на страницу">
                                    <b-select v-model="per_page" expanded>
                                        <option v-for="item in pre_page_a" :value="item">{{ item }}</option>
                                    </b-select>
                                </b-field>
                            </div>
                            <div class="col-md-4">
                                <b-field
                                    label="Поиск">
                                    <div class="field has-addons">
                                        <div class="control" style="width: 100%;">
                                            <input class="input" type="text" v-model="filter" placeholder="Поиск">
                                        </div>
                                        <div class="control">
                                            <a class="button is-primary is-outlined" @click="onSearch(filter)">
                                                Найти
                                            </a>
                                        </div>
                                    </div>
                                </b-field>
                            </div>
                            <div class="col-md-4">
                                <button class="button is-primary is-smal" style="float: right; margin-bottom: 10px; margin-top: 15px;"
                                        @click="isCardModalActive = true">
                                    Добавить Текст
                                </button>
                                <b-modal :active.sync="isCardModalActive" :width="960" scroll="keep">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="content">
                                                <?php echo $__env->make('admin.text._form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </b-modal>
                            </div>
                        </div>
                    </div>
                </header>
                <b-table
                    :data="data"
                    paginated
                    hoverable
                    mobile-cards
                    :filter="filter"
                    :per-page="per_page"
                    :opened-detailed="defaultOpenedDetails"
                    detailed
                    detail-key="id">
                    <template slot-scope="props">

                        <b-table-column field="id" label="id" width="40" sortable numeric>
                            <b>{{ props.row.id }}</b>
                        </b-table-column>

                        <b-table-column field="page" label="Страница" sortable numeric>
                            <code>{{ props.row.page }}</code>
                        </b-table-column>

                        <b-table-column field="key" label="Ключ" sortable numeric>
                            <code>{{ props.row.key }}</code>
                        </b-table-column>

                        <b-table-column field="created_at" label="Дата" sortable numeric>
                            {{ props.row.created_at }}
                        </b-table-column>


                        <b-table-column field="action" label="" sortable numeric>
                            <div class="btn-group">
                                <a :href="routeEdit+ '/' + props.row.id + '/edit'"
                                   class="btn btn-outline-primary btn-sm">EDIT</a>
                                <button @click="onDelete(props.row.id)" class="btn btn-outline-danger btn-sm">DELETE
                                </button>
                            </div>
                        </b-table-column>
                    </template>
                    <template slot="empty">
                        <section class="section">
                            <div class="content has-text-grey has-text-centered">
                                <p>
                                    <b-icon
                                        icon="emoticon-sad"
                                        size="is-large">
                                    </b-icon>
                                </p>
                                <p>Ничего нет.</p>
                            </div>
                        </section>
                    </template>
                    <template slot="detail" slot-scope="props">
                        <article class="media">
                            <div class="media-content">
                                <div class="content">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4>Текст</h4>
                                                <span v-html="props.row.text"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </template>
                </b-table>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startPush('scripts'); ?>
    <script>
        var lfm = function (options, cb) {

            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=1280,height=600');
            window.SetUrl = cb;
        }
    </script>
    <script>
        new Vue({
            el: "#content",
            data: {
                errors: {},
                defaultOpenedDetails: [],
                routeEdit: route('admin.texts.index'),
                filter: '',
                per_page: 10,
                pre_page_a: [5, 10, 20],
                text: {},
                isCardModalActive: false,
                data: <?php echo json_encode($texts, 15, 512) ?>,
            },
            methods: {
                onDelete(index) {
                    let self = this;
                    axios.delete(route('admin.texts.destroy', index))
                        .then(function (response) {
                            self.data = response.data;
                            self.categories = response.data;
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Текст удалена`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время удаления произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                },
                onSave() {
                    let self = this;
                    axios.post(route('admin.texts.store'), self.text)
                        .then(function (response) {
                            self.data = response.data;
                            self.text = {};
                            self.errors = {};
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Текст успешно сохранен`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            let response = error.response;
                            if(response.status === 422) {

                                self.errors = response.data.errors;
                                Object.keys(response.data.errors).map((key) => {
                                    self.errors[key] = response.data.errors[key][0];
                                });
                            }
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                },
                onSearch(search) {
                    let self = this;
                    axios.post(route('admin.texts.search'), {
                        search: search
                    })
                        .then(function (response) {
                            self.data = response.data;
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Поиск прошел успешно`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время поиска произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>