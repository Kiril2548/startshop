<?php $__env->startSection('content'); ?>


    <div class="container mb-2 mt-2">
        <div class="row">
            <div class="col-md-9">
                <div class="card img-card">
                    <div class="img"
                         style="
                             min-height: 400px;
                             background-size: cover;
                             background-position: center;
                             background-repeat: no-repeat;
                             background-image: url(<?php echo e($news->img); ?>);
                             "></div>
                </div>
                <div class="card card-info p-4 text-justify" style="margin-top: -150px; width: 94%; margin-left: 3%;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 text-left"><h3><?php echo e($news->title); ?></h3></div>
                            <div class="col-md-6 text-right">
                                <span>
                                    <span class="mr-3"><i class="fas fa-eye"></i> <?php echo e($news->views); ?></span>
                                    <span><i class="fas fa-clock"></i> <?php echo e($news->created_at); ?></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <p><?php echo e($news->text); ?></p>
                </div>
            </div>
            <div class="col-md-3">
                <ul class="list-group">
                    <?php $__currentLoopData = $news_all; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li class="list-group-item">
                            <a href="<?php echo e(route('news.show', $item->slug)); ?>" style="
                                display: block;
                                text-align: right;
                                height: 100%;
                                color: #666;
                                text-decoration: none;
                                padding: 10px;
                            "><?php echo e($item->title); ?></a>
                        </li>

                        <?php if($key == 10): ?>
                            <?php break; ?>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>