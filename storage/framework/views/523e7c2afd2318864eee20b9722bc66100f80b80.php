<?php $__env->startSection('content'); ?>

    <div class="container mt-3">
        <div class="row">
            <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-12 mb-4">
                    <div class="card">
                        <div class="img p-3">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-3" style="
                                        background-image: url(<?php echo e($new->img); ?>);
                                        background-size: cover;
                                        background-position: center;
                                        "></div>
                                    <div class="col-md-9">
                                        <h3 class="font-weight-bold"><?php echo e($new->title); ?></h3>
                                        <p><?php echo substr($new->text, 0, 500); ?></p>
                                        <div class="d-flex justify-content-end pl-3 pr-3">
                                            <span><i class="fas fa-clock"></i> <?php echo e($new->created_at); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <div class="w-100 d-flex justify-content-center">
                <?php echo e($news->links()); ?>

            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>