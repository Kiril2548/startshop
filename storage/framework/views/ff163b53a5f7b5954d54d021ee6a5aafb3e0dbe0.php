<?php $__env->startSection('content'); ?>

    <?php if($page->img != "" || $page->img != null): ?>
        <div class="container-fluid img-card">
            <div class="row">
                <div class="col-md-12 text-center"
                     style="height: 400px; padding: 75px; background-image: url(<?php echo e($page->img); ?>); background-size: cover;">
                    <h1 class="text-center"
                        style="
                            font-size: 70px;
                            font-weight: 800;
                            background-color: rgba(21,30,18,0.7);
                            padding: 15px;
                            border-radius: 15px;
                        "
                    ><?php echo e($page->title); ?></h1>
                </div>
            </div>
        </div>
        <div class="container card card-info p-4" style="margin-top: -100px;">
            <div class="row">
                <div class="col-md-12 text-justify">
                    <p><?php echo e($page->page_text); ?></p>
                </div>
            </div>
        </div>

    <?php else: ?>
        <div class="container mt-3">
            <div class="row">
                <div class="col-md-12 text-justify">
                    <h1 class="text-center"
                        style="
                            font-size: 70px;
                            font-weight: 800;
                            padding: 15px;
                        "
                    ><?php echo e($page->title); ?></h1>

                    <p><?php echo e($page->page_text); ?></p>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>