<style>
    #circle-div{
        position: fixed;
        background-color: #151E12;
        width: 100%;
        height: 100vh;
        z-index: 10000;
    }

    #circle {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        width: 150px;
        height: 150px;
    }

    .loader {
        width: calc(100% - 0px);
        height: calc(100% - 0px);
        border: 8px solid #6BA73B;
        border-top: 8px solid #1C2519;
        border-radius: 50%;
        animation: rotate 5s linear infinite;
    }

    @keyframes  rotate {
        100% {transform: rotate(360deg);}
    }

    *{
        transition: all 0.2s ease;
    }

    html, body {
        background-color: #151E12;
        color: #7BC043;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        height: 100vh;
        margin: 0;
    }

    ::-webkit-scrollbar{
        width:6px;
    }
    ::-webkit-scrollbar-thumb{
        border-width:1px 1px 1px 2px;
        border-color: #777;
        background-color: #18621a;
    }
    ::-webkit-scrollbar-thumb:hover{
        border-width: 1px 1px 1px 2px;
        border-color: #555;
        background-color: #7BC043;
    }
    ::-webkit-scrollbar-track{
        border-width:0;
        border-left: solid 1px #151E12;
        background-color: #151E12;
    }
    ::-webkit-scrollbar-track:hover{
        border-left: solid 1px #151E12;
        background-color: #1C2519;
    }

    .card-shop{
        transition: all 0.5s ease;
    }
    .card-shop:hover{
        transform: scale(1.02);
        box-shadow: 0px 0px 30px rgba(123, 192, 67, 0.26);
    }

    .list-group-item {
        position: relative;
        display: block;
        padding: 0px;
        margin-bottom: -1px;
        background-color: #1C2519;
        border: 1px solid #131d10;
    }

    .modal-content {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        width: 100%;
        pointer-events: auto;
        background-color: #151E12;
        background-clip: padding-box;
        border: 1px solid #1C2519;
        border-radius: .3rem;
        outline: 0;
    }
    .modal-header {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: start;
        align-items: flex-start;
        -ms-flex-pack: justify;
        justify-content: space-between;
        padding: 1rem;
        border-bottom: 1px solid #1C2519;
        border-top-left-radius: .3rem;
        border-top-right-radius: .3rem;
    }

    .form-control, .form-control:active, .form-control:hover, .form-control:focus {
        color: #7BC043;
        background-color: #30392D;
        border: 1px solid #80906f;
        outline: none;

    }
    .form-control:active, .form-control:focus {
        box-shadow: 0 0 0 0.2rem rgba(128, 144, 111, 0.31);
    }

    a {
        color: #7BC043 !important;
    }

    a:hover {
        color: #b1f946 !important;
    }

    .card {
        background-color: #1C2519;
        border: 1px solid rgba(0, 0, 0, 0.125);
        border-radius: 0.25rem;
    }

    .navbar-laravel {
        background-color: #1C2519;
        color: #7BC043 !important;
    }

    .dropdown-menu {
        background-color: #1C2519;
        border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.25rem;
    }

    .nav-item:hover .dropdown-menu {
        top: 90% !important;
        display: block !important;
    }

    .dropdown-menu:hover {
        top: 90% !important;
        display: block !important;
    }

    .dropdown-item:hover {
        background-color: #2D362A;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 84px;
    }
    .img-card:hover + .card-info{
        margin-top: -10px !important;
    }
    .back-to-top {
        cursor: pointer;
        position: fixed;
        bottom: 20px;
        right: 20px;
        display:none;
    }



    .comment-box {
        margin-top: 30px !important;
        width: 100%;
    }

    .comment-box img {
        width: 50px;
        height: 50px;
    }
    .comment-box .media-left {
        padding-right: 10px;
        width: 65px;
    }
    .comment-box .media-body p {
        border: 1px solid #2c3529;
        padding: 10px;
    }
    .comment-box .media-body .media p {
        margin-bottom: 0;
    }
    .comment-box .media-heading {
        background-color: #151E12;
        border: 1px solid #2c3529;
        padding: 7px 10px;
        position: relative;
        margin-bottom: -1px;
    }
    .comment-box .media-heading:before {
        content: "";
        width: 12px;
        height: 12px;
        background-color: #151E12;
        border: 1px solid #151E12;
        border-width: 1px 0 0 1px;
        -webkit-transform: rotate(-45deg);
        transform: rotate(-45deg);
        position: absolute;
        top: 10px;
        left: -6px;
    }

    .page-link {
        background-color: #1C2519;
        border: 1px solid #7BC043;
    }

    .page-item.active .page-link {
        z-index: 1;
        color: #fff;
        background-color: #7BC043;
        border-color: #7BC043;
    }

    .page-item.disabled .page-link {
        color: #6c757d;
        background-color: #333;
        border-color: #7BC043;
    }

    .page-link:hover {
        background-color: #576054;
        border-color: #7BC043;
    }
</style>
