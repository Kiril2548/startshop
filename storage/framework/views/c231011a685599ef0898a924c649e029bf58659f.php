<div class="footer" style="background-color: #1C2519;">
    <div class="text-center">
        <p style="padding: 0; margin: 0;">
            <strong><img src="<?php echo e(asset('favicon.ico')); ?>" width="35" height="35" style="margin-right: 5px;"> <?php echo e(config('app.name', 'MineStartProject')); ?> /  v0.0.1 (alpha)</strong>
        </p>
    </div>
</div>
