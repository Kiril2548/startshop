<?php $__env->startSection('content'); ?>

    <div class="container mt-3">
        <div class="row">
            <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-12 mb-4">
                    <div class="card">
                        <div class="img"
                             style="min-height: 500px; background-size: cover; background-image: url(<?php echo e($new->img); ?>)">
                            <div style="
                                position: absolute;
                                bottom: 0;
                                width: 100%;
                                background-color: rgba(21,30,18,0.9);
                                min-height: 150px;
                                padding: 15px;

                            ">
                                <h3 class="font-weight-bold"><a
                                        href="<?php echo e(route('news.show', $new->slug)); ?>"><?php echo e($new->title); ?></a></h3>
                                <p><?php echo substr($new->text, 0, 500); ?>...<a
                                        style="font-weight: 600; text-decoration: none;"
                                        href="<?php echo e(route('news.show', $new->slug)); ?>">Перейти <i
                                            class="fas fa-arrow-circle-right"></i></a></p>
                                <div class="d-flex justify-content-between pl-3 pr-3">
                                    <span><i class="fas fa-eye"></i> <?php echo e($new->views); ?></span>
                                    <span><i class="fas fa-clock"></i> <?php echo e($new->created_at); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <div class="w-100 d-flex justify-content-center">
                <?php echo e($news->links()); ?>

            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>