<?php $__env->startPush('styles'); ?>
    <style>
        .img-preview-table:hover {
            cursor: pointer;
        }
    </style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="notification row">

            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="<?php echo e(route('admin.main')); ?>">Админка</a></li>
                        <li><a href="<?php echo e(route('admin.sessions.index')); ?>">Сесии</a></li>
                    </ul>
                </nav>
            </div>

            <div class="card col-md-12" style="padding: 5px;">
                <header class="card-header" style="padding: 0; padding-bottom: 5px; padding-top: 5px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <b-field
                                    label="Позицый на страницу">
                                    <b-select v-model="per_page" expanded>
                                        <option v-for="item in pre_page_a" :value="item">{{ item }}</option>
                                    </b-select>
                                </b-field>
                            </div>
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>
                    </div>
                </header>
                <b-table
                    :data="data"
                    paginated
                    hoverable
                    mobile-cards
                    :filter="filter"
                    :per-page="per_page"
                    :opened-detailed="defaultOpenedDetails">
                    <template slot-scope="props">
                        <b-table-column field="user" label="Пользователь" width="70" sortable>
                            <span v-if="props.row.user != null">
                                <a :href="routeUser+'/'+props.row.user.id+'/edit'">{{ props.row.user.name }}</a>
                            </span>
                            <span v-if="props.row.user == null">NA</span>
                        </b-table-column>

                        <b-table-column field="ip_address" label="IP" sortable>
                            <p>{{ props.row.ip_address }}</p>
                        </b-table-column>

                        <b-table-column field="user_agent" label="Информация" sortable>
                            <p class="text-center">{{ props.row.user_agent }}</p>
                        </b-table-column>

                        <b-table-column field="last_activity" label="Активность" sortable>
                            {{ props.row.last_activity }}
                        </b-table-column>

                    </template>
                    <template slot="empty">
                        <section class="section">
                            <div class="content has-text-grey has-text-centered">
                                <p>
                                    <b-icon
                                        icon="emoticon-sad"
                                        size="is-large">
                                    </b-icon>
                                </p>
                                <p>Ничего нет.</p>
                            </div>
                        </section>
                    </template>
                </b-table>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startPush('scripts'); ?>
    <script>
        var vue = new Vue({
            el: "#content",
            data: {
                routeUser: <?php echo json_encode(route('admin.users.index'), 15, 512) ?>,
                filter: '',
                per_page: 10,
                pre_page_a: [5, 10, 20, 50, 100],
                user: {},
                defaultOpenedDetails: [],
                isCardModalActive: false,
                data: <?php echo json_encode($sessions, 15, 512) ?>,
            }
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>