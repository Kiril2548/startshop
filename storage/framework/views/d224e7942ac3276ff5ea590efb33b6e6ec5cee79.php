<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="notification row">
            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="<?php echo e(route('admin.main')); ?>">Админка</a></li>
                        <li><a href="<?php echo e(route('admin.article-categories.index')); ?>">Категории (Статьи)</a></li>
                        <li><a href="#">Порядок показа категорий</a></li>
                    </ul>
                </nav>
            </div>

            <div class="card col-md-12" style="padding: 5px;">
                <header class="card-header" style="padding: 0; padding-bottom: 5px; padding-top: 5px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3">
                                <b-field
                                    label="Позицый на страницу">
                                    <b-select v-model="per_page" expanded>
                                        <option v-for="item in pre_page_a" :value="item">{{ item }}</option>
                                    </b-select>
                                </b-field>
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3">

                            </div>
                        </div>
                    </div>
                </header>
                <b-table
                    :data="data"
                    paginated
                    hoverable
                    mobile-cards
                    :per-page="per_page">
                    <template slot-scope="props">

                        <b-table-column field="id" label="ID" width="40" sortable numeric>
                            {{ props.row.id }}
                        </b-table-column>

                        <b-table-column field="title" label="Категория" sortable>
                            {{ props.row.title }}
                        </b-table-column>

                        <b-table-column field="category_id" label="Влож. Категория" sortable>
                            <span v-if="props.row.category_id != null">{{ props.row.category.title }}</span>
                            <span v-if="props.row.category_id == null">NA</span>
                        </b-table-column>

                        <b-table-column field="status" label="Статус" sortable centered>
                            <span class="tag is-success" v-if="props.row.status == 1">
                                ACTIVE
                            </span>
                            <span class="tag is-danger" v-if="props.row.status == 0">
                                DELETE
                            </span>
                        </b-table-column>

                        <b-table-column field="action" label="" sortable numeric>
                            <div class="field has-addons">
                                <div class="control">
                                    <input class="input" type="text" v-model="props.row.place" placeholder="Порядковый номер">
                                </div>
                                <div class="control">
                                    <a class="button is-primary is-outlined" @click="onSave(props.row.slug, props.row.place)">
                                        Сохранить
                                    </a>
                                </div>
                            </div>
                        </b-table-column>
                    </template>
                    <template slot="empty">
                        <section class="section">
                            <div class="content has-text-grey has-text-centered">
                                <p>
                                    <b-icon
                                        icon="emoticon-sad"
                                        size="is-large">
                                    </b-icon>
                                </p>
                                <p>Ничего нет.</p>
                            </div>
                        </section>
                    </template>
                </b-table>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startPush('scripts'); ?>
    <script>
        new Vue({
            el: "#content",
            data: {
                per_page: 50,
                pre_page_a: [50, 100, 200],
                data: <?php echo json_encode($categories, 15, 512) ?>,
            },
            methods: {
                onSave(slug, place) {
                    let self = this;
                    axios.put(route('admin.article-categories.update', slug), {
                        place: place,
                    })
                        .then(function (response) {
                            self.$snackbar.open({
                                duration: 3000,
                                message: `Порядновый номер успешно изменен`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                        })
                        .catch(function (error) {
                            self.$toast.open({
                                duration: 2000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>