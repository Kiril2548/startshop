<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="notification">
            DashBoard
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startPush('scripts'); ?>
    <script>
        new Vue({
            el: "#content",
            data: {
                m: "HELLO"
            }
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>