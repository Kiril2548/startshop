<?php $__env->startSection('content'); ?>

    <div class="container mt-3">
        <div class="row">
            <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-12 mb-4">
                    <div class="card">
                        <div class="img p-3">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-3" style="
                                        background-image: url(<?php echo e($new->img); ?>);
                                        background-size: cover;
                                        background-position: center;
                                        "></div>
                                    <div class="col-md-9">
                                        <h3 class="font-weight-bold"><a
                                                href="<?php echo e(route('news.show', $new->slug)); ?>"><?php echo e($new->title); ?></a></h3>
                                        <p><?php echo substr($new->text, 0, 500); ?>...<a
                                                style="font-weight: 600; text-decoration: none;"
                                                href="<?php echo e(route('news.show', $new->slug)); ?>">Перейти
                                                <i class="fas fa-arrow-circle-right"></i></a></p>
                                        <div class="d-flex justify-content-between pl-3 pr-3">
                                            <span><i class="fas fa-eye"></i> <?php echo e($new->views); ?></span>
                                            <span><i class="fas fa-clock"></i> <?php echo e($new->created_at); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <div class="w-100 d-flex justify-content-center">
                <?php echo e($news->links()); ?>

            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>