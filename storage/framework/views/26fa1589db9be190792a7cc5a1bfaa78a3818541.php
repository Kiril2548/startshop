<div class="page-wrapper chiller-theme toggled">
    <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
        <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar" class="sidebar-wrapper">
        <div class="sidebar-content">
            <div class="sidebar-brand">
                <a href="<?php echo e(route('main')); ?>"><img src="<?php echo e(asset('favicon.ico')); ?>" width="35" height="35"
                                                         style="margin-right: 5px;"> AdminPanel</a>
                <div id="close-sidebar">
                    <i class="fas fa-times"></i>
                </div>
            </div>
            <div class="sidebar-header">
                <div class="user-pic">
                    <img class="img-responsive img-rounded" src="<?php echo e(Auth::user()->avatar); ?>" alt="User picture">
                </div>
                <div class="user-info">
                        <span class="user-name"><?php echo e(Auth::user()->name); ?>

                            <strong>_</strong>
                        </span>
                    <span
                        class="user-role"><?php echo e(Auth::user()->roles[count((Auth::user()->roles))-1]->display_name); ?></span>
                    <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                </div>
            </div>
            <!-- sidebar-header  -->
        
        
        
        
        
        
        
        
        
        
        
        
        <!-- sidebar-search  -->
            <div class="sidebar-menu">
                <ul>
                    <li class="header-menu">
                        <span>Default</span>
                    </li>
                    <?php if($admin_settings['e_commerce_d'] == 1 || $admin_settings['blog_d'] == 1): ?>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fa fa-tachometer-alt"></i>
                                <span>Админка</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <?php if($admin_settings['e_commerce_d'] == 1): ?>
                                        <li>
                                            <a href="<?php echo e(route('admin.main')); ?>">E-commerce</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if($admin_settings['blog_d'] == 1): ?>
                                        <li>
                                            <a href="<?php echo e(route('admin.main')); ?>">Blog</a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="<?php echo e(route('admin.users.index')); ?>">
                            <i class="fa fa-users"></i>
                            <span>Пользователи</span>
                        </a>
                    </li>

                    <?php if($admin_settings['session'] == 1): ?>
                        <li>
                            <a href="<?php echo e(route('admin.sessions.index')); ?>">
                                <i class="fa fa-globe"></i>
                                <span>Сессии</span>
                            </a>
                        </li>
                    <?php endif; ?>


                    <?php if(
                        $admin_settings['category'] == 1
                        || $admin_settings['products'] == 1
                        || $admin_settings['orders'] == 1
                        || $admin_settings['payments'] == 1
                        || $admin_settings['carts'] == 1
                    ): ?>
                        <li class="header-menu">
                            <span>E-commerce</span>
                        </li>

                        <?php if($admin_settings['category'] == 1): ?>
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fa fa-clipboard-list"></i>
                                    <span>Категории (продукты)</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="<?php echo e(route('admin.categories.index')); ?>">Все категории</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo e(route('admin.categories.place')); ?>">Позицыи Категорий</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        <?php endif; ?>

                        <?php if($admin_settings['products'] == 1): ?>
                            <li>
                                <a href="<?php echo e(route('admin.products.index')); ?>">
                                    <i class="fa fa-briefcase"></i>
                                    <span>Продукты</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if($admin_settings['orders'] == 1): ?>
                            <li>
                                <a href="#">
                                    <i class="fa fa-archive"></i>
                                    <span>Заказы</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if($admin_settings['payments'] == 1): ?>
                            <li>
                                <a href="#">
                                    <i class="fa fa-credit-card"></i>
                                    <span>Транзакции</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if($admin_settings['carts'] == 1): ?>
                            <li>
                                <a href="#">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span>Карзина</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if(
                        $admin_settings['pages'] == 1
                        || $admin_settings['news'] == 1
                        || $admin_settings['articles'] == 1
                        || $admin_settings['texts'] == 1
                    ): ?>
                        <li class="header-menu">
                            <span>Content</span>
                        </li>

                        <?php if($admin_settings['pages'] == 1): ?>
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fa fa-file-alt"></i>
                                    <span>Страницы</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="<?php echo e(route('admin.menus.index')); ?>">Меню</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo e(route('admin.pages.index')); ?>">Страницы</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        <?php endif; ?>

                        <?php if($admin_settings['news'] == 1): ?>
                            <li>
                                <a href="<?php echo e(route('admin.news.index')); ?>">
                                    <i class="fa fa-newspaper"></i>
                                    <span>Новости</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if($admin_settings['article_categories'] == 1): ?>
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fa fa-clipboard-list"></i>
                                    <span>Категории (статьи)</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="<?php echo e(route('admin.article-categories.index')); ?>">Все категории</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo e(route('admin.article-categories.place')); ?>">Позицыи Категорий</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        <?php endif; ?>

                        <?php if($admin_settings['articles'] == 1): ?>
                            <li>
                                <a href="<?php echo e(route('admin.articles.index')); ?>">
                                    <i class="fa fa-bookmark"></i>
                                    <span>Статьи</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if($admin_settings['texts'] == 1): ?>
                            <li>
                                <a href="<?php echo e(route('admin.texts.index')); ?>">
                                    <i class="fa fa-font"></i>
                                    <span>Текста</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if(Auth::user()->hasRole('admin')): ?>
                        <?php if(
                            $admin_settings['api_settings'] == 1
                            || $admin_settings['settings'] == 1
                            || $admin_settings['roles'] == 1
                            || $admin_settings['permissions'] == 1
                        ): ?>
                            <li class="header-menu">
                                <span>Settings</span>
                            </li>

                            <?php if($admin_settings['settings'] == 1): ?>
                                <li>
                                    <a href="<?php echo e(route('admin.settings.index')); ?>">
                                        <i class="fa fa-cog"></i>
                                        <span>Настройки</span>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if($admin_settings['api_settings'] == 1): ?>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-lock"></i>
                                        <span>Апи настрайки</span>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if($admin_settings['roles'] == 1): ?>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user"></i>
                                        <span>Роли</span>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if($admin_settings['permissions'] == 1): ?>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-terminal"></i>
                                        <span>Права</span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if(Auth::user()->hasRole('owner')): ?>
                        <li class="header-menu">
                            <span>Misc</span>
                        </li>
                        <li>
                            <a href="<?php echo e(route('admin.documentation')); ?>">
                                <i class="fa fa-question"></i>
                                <span>Documentation</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo e(route('admin.admin_settings.index')); ?>">
                                <i class="fa fa-cogs"></i>
                                <span>Настройки админки</span>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
            <!-- sidebar-menu  -->
        </div>
        <!-- sidebar-content  -->

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    </nav>
    <!-- sidebar-wrapper  -->
</div>
<!-- page-wrapper -->
