<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="notification row">
            <div class="col-md-12">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="<?php echo e(route('admin.main')); ?>">Админка</a></li>
                        <li><a href="<?php echo e(route('admin.categories.index')); ?>">Категории (Продукты)</a></li>
                        <li><a href="#">Редактирование категории :<b><?php echo e($category->title); ?></b></a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card" style="padding: 15px;"><?php echo $__env->make('admin.category._form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?></div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startPush('scripts'); ?>
    <script>
        var slug = <?php echo json_encode($category->slug, 15, 512) ?>;
        new Vue({
            el: "#content",
            data: {
                errors: {},
                category: <?php echo json_encode($category, 15, 512) ?>,
                categories: <?php echo json_encode($categories, 15, 512) ?>
            },
            methods: {
                onSave() {
                    let self = this;
                    axios.put(route('admin.categories.update', self.category.slug), self.category)
                        .then(function (response) {
                            self.errors = {};
                            self.$snackbar.open({
                                duration: 5000,
                                message: `Категория успешно обновлена`,
                                position: 'is-bottom-left',
                                queue: false,
                            })
                            setTimeout(function () {
                                if(response.data.slug != slug){
                                    console.log(response.data.slug + ' -> ' + slug);
                                    window.location = route('admin.categories.edit', response.data.slug);
                                }
                            }, 1500);
                        })
                        .catch(function (error) {
                            self.errors= {};
                            let response = error.response;
                            if(response.status === 422) {

                                self.errors = response.data.errors;
                                Object.keys(response.data.errors).map((key) => {
                                    self.errors[key] = response.data.errors[key][0];
                                });
                            }
                            self.$toast.open({
                                duration: 5000,
                                message: `Во время сохранения произошла какая-то ошибка !`,
                                position: 'is-bottom',
                                type: 'is-danger',
                                queue: false,
                            })
                        });
                }
            }
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>